﻿using Hangfire;
using Hangfire.SqlServer;
using Microsoft.Owin;
using Owin;
using System;
using System.Collections.Generic;
using System.Configuration;

[assembly: OwinStartup(typeof(TecoAPI.Startup))]
namespace TecoAPI
{
    public partial class Startup
    {
        private IEnumerable<IDisposable> GetHangfireServers()
        {
            GlobalConfiguration.Configuration
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings()
                .UseSqlServerStorage(ConfigurationManager.ConnectionStrings["TecoAppHangfire"].ConnectionString, new SqlServerStorageOptions
                {
                    CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                    SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                    QueuePollInterval = TimeSpan.Zero,
                    UseRecommendedIsolationLevel = true,
                    DisableGlobalLocks = true
                });

            yield return new BackgroundJobServer();
        }

        public void Configuration(IAppBuilder app)
        {
            app.UseHangfireAspNet(GetHangfireServers);
            app.UseHangfireDashboard();
            //app.UseHangfireServer();

            #region 排程(已註解，設定請至ScheduleController)

            //BatchController Batch = new BatchController();
            //RecurringJob.AddOrUpdate(() => Batch.Daily2Hour(), "0 0,2,4,6,8,10,12,14,16,18,20,22 * * *");
            //RecurringJob.AddOrUpdate(() => Batch.Daily5Minute(), "0,5,10,15,20,25,30,35,40,45,50,55 * * * *");

            #region Sample

            // fire and got:站台啟動後只會執行一次
            //BackgroundJob.Enqueue(() => Console.WriteLine("Fire and forgot"));

            // delay: 設定時間間隔，每隔時間間隔執行一次
            //BackgroundJob.Schedule(() => Console.WriteLine("Delayed"), TimeSpan.FromDays(1));

            // recurring: 設定cron敘述，重複執行多次
            //RecurringJob.AddOrUpdate(() => Console.WriteLine("Daily Job"), Cron.Daily);

            // continue: 在某個job執行完後接續執行
            //var id = BackgroundJob.Enqueue(() => Console.WriteLine("Hello, "));
            //BackgroundJob.ContinueWith(id, () => Console.WriteLine("world!"));

            #endregion Sample

            #endregion 排程
        }
    }
}