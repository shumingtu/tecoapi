﻿using NLog;
using System;
using System.Linq;
using System.Web.Http;

namespace TecoAPI.Controllers
{
    public class BatchController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        [HttpGet]
        public static bool Base()
        {
            bool resultValue = false;

            try
            {
                //客戶
                var KNA1Controller = new KNA1Controller();
                KNA1Controller.Update();

                //商品
                var MARAController = new MARAController();
                MARAController.Update();

                //信限
                var CUSTCREDITController = new CUSTCREDITController();
                CUSTCREDITController.Update();

                //價格
                var CUSTPRICEController = new CUSTPRICEController();
                CUSTPRICEController.Update();

                //庫存
                var MARDController = new MARDController();
                MARDController.Update();

                resultValue = true;
            }
            catch (Exception ex)
            {
                logger.Error("/Batch/Base - " + ex.Message);
            }

            return resultValue;
        }

        [HttpGet]
        public static bool KNA1()
        {
            bool resultValue = false;

            try
            {
                //客戶
                var KNA1Controller = new KNA1Controller();
                KNA1Controller.Update();

                resultValue = true;
            }
            catch (Exception ex)
            {
                logger.Error("/Batch/KNA1 - " + ex.Message);
            }

            return resultValue;
        }

        [HttpGet]
        public static bool MARA()
        {
            bool resultValue = false;

            try
            {
                //商品
                var MARAController = new MARAController();
                MARAController.Update();

                resultValue = true;
            }
            catch (Exception ex)
            {
                logger.Error("/Batch/MARA - " + ex.Message);
            }

            return resultValue;
        }

        [HttpGet]
        public static bool CUSTCREDIT()
        {
            bool resultValue = false;

            try
            {
                //信限
                var CUSTCREDITController = new CUSTCREDITController();
                CUSTCREDITController.Update();

                resultValue = true;
            }
            catch (Exception ex)
            {
                logger.Error("/Batch/CUSTCREDIT - " + ex.Message);
            }

            return resultValue;
        }

        [HttpGet]
        public static bool CUSTPRICE()
        {
            bool resultValue = false;

            try
            {
                //價格
                var CUSTPRICEController = new CUSTPRICEController();
                CUSTPRICEController.Update();

                resultValue = true;
            }
            catch (Exception ex)
            {
                logger.Error("/Batch/CUSTPRICE - " + ex.Message);
            }

            return resultValue;
        }

        [HttpGet]
        public static bool MARD()
        {
            bool resultValue = false;

            try
            {
                //庫存
                var MARDController = new MARDController();
                MARDController.Update();

                resultValue = true;
            }
            catch (Exception ex)
            {
                logger.Error("/Batch/MARD - " + ex.Message);
            }

            return resultValue;
        }

        [HttpGet]
        public static bool Order()
        {
            bool resultValue = false;

            try
            {
                //訂單
                var OrderController = new OrderController();
                OrderController.Batch();

                resultValue = true;
            }
            catch (Exception ex)
            {
                logger.Error("/Batch/Order - " + ex.Message);
            }

            return resultValue;
        }
    }
}
