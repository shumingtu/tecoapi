﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using TecoAPI.Services;
using TecoModels;

namespace TecoAPI.Controllers
{
    [EnableCors("*", "*", "*")]
    public class ConfigController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region 後臺

        #region 查詢Config資料

        [HttpGet]
        [CorsHandle]
        public async Task<List<ConfigQueryResponseModel>> Query()
        {
            var ConfigResponse = new List<ConfigQueryResponseModel>();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var ConfigList = await TecoApp.Config.AsNoTracking().ToListAsync();

                foreach (var Config in ConfigList)
                {
                    if (Config.Code.Contains("Credit"))
                    {
                        ConfigResponse.Add(new ConfigQueryResponseModel()
                        {
                            ID = Config.ID,
                            Code = Config.Code,
                            Value = Config.Value,
                            Name = Config.Name,
                            Enable = (Config.Enable == "Y" ? "1" : "0")
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("/Config/Query - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return ConfigResponse;
        }

        #endregion 查詢Config資料

        #region 修改Config資料

        [HttpPut]
        [CorsHandle]
        public async Task<List<ConfigQueryResponseModel>> Modify()
        {
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var ConfigPath = await TecoApp.Config.Where(p => p.Code == "RootFilePath" && p.Enable == "Y").AsNoTracking().ToListAsync();
                var RootFilePath = ConfigPath.FirstOrDefault().Value + "Config\\";

                if (!Request.Content.IsMimeMultipartContent())
                {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                }

                if (!Directory.Exists(RootFilePath))
                {
                    Directory.CreateDirectory(RootFilePath);
                }

                var StreamProvider = new CustomMultipartFormDataStreamProvider(RootFilePath);

                await Request.Content.ReadAsMultipartAsync(StreamProvider);

                var ConfigModifyRequest = new ConfigModifyRequestModel();

                foreach (var Key in StreamProvider.FormData.AllKeys)
                {
                    foreach (var Value in StreamProvider.FormData.GetValues(Key))
                    {
                        ConfigModifyRequest = JsonConvert.DeserializeObject<ConfigModifyRequestModel>(Value);
                    }
                }

                var Config = await TecoApp.Config.Where(x => x.ID == ConfigModifyRequest.ID).FirstOrDefaultAsync();
                Config.Code = ConfigModifyRequest.Code.Trim();
                Config.Value = ConfigModifyRequest.Value.Trim();
                Config.Name = ConfigModifyRequest.Name.Trim();
                Config.Enable = (ConfigModifyRequest.Enable.Trim() == "1" ? "Y" : "N");
                Config.UPDATE_TIME = System.DateTime.Now;
                Config.UPDATE_USER = "System";
                TecoApp.Entry(Config).State = EntityState.Modified;

                await TecoApp.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                logger.Error("/Config/Modify - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return await Query();
        }

        #endregion 修改Config資料

        #endregion 後臺
    }
}
