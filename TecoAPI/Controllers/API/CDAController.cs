﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TecoModels;
using System.IO;
using CsvHelper;
using TecoAPI.Services;
using System.Threading.Tasks;
using NLog;
using System.Data.Entity;

namespace TecoAPI.Controllers
{
    public class CDAController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region 後臺

        #region 匯入客戶部門歸屬資料

        [HttpPost]
        [CorsHandle]
        public async Task<List<CDAResponseModel>> Import()
        {
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var Config = await TecoApp.Config.Where(p => p.Code == "RootFilePath" && p.Enable == "Y").AsNoTracking().ToListAsync();
                var RootFilePath = Config.FirstOrDefault().Value + "CDA\\";

                if (!Request.Content.IsMimeMultipartContent())
                {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                }

                if (!Directory.Exists(RootFilePath))
                {
                    Directory.CreateDirectory(RootFilePath);
                }

                var StreamProvider = new CustomMultipartFormDataStreamProvider(RootFilePath);

                await Request.Content.ReadAsMultipartAsync(StreamProvider);

                foreach (MultipartFileData File in StreamProvider.FileData)
                {
                    string FileName = File.Headers.ContentDisposition.FileName;
                    string FileNameNew = Path.GetFileName(File.LocalFileName);
                    string FilePath;
                    if (FileName.StartsWith("\"") && FileName.EndsWith("\""))
                    {
                        FileName = FileName.Trim('"');
                    }
                    if (FileName.Contains(@"/") || FileName.Contains(@"\"))
                    {
                        FileName = Path.GetFileName(FileName);
                    }
                    FilePath = RootFilePath + FileNameNew;

                    using (var StreamReader = new StreamReader(FilePath))
                    using (var CsvReader = new CsvReader(StreamReader))
                    {
                        CsvReader.Configuration.HasHeaderRecord = false; //不抓欄位名稱，中文會有問題
                        CsvReader.Read(); //跳過第一行欄位名稱
                        var CDAUpload = CsvReader.GetRecords<CDAUploadModel>().ToList();

                        var CDAList = TecoApp.CustomerDepartmentAttribution.ToList();
                        //var OldFirstID = (CDAList.Count() == 0 ? 0 : Convert.ToInt32(CDAList.FirstOrDefault().ID));
                        //var OldLastID = (CDAList.Count() == 0 ? 0 : Convert.ToInt32(CDAList.LastOrDefault().ID));

                        TecoApp.CustomerDepartmentAttribution.RemoveRange(CDAList);
                        TecoApp.SaveChanges();

                        logger.Debug("/CustomerDepartmentAttribution/Import - RemoveRangeSuccess");

                        //設定新增的ID初始值
                        int ID = 1;
                        //if (OldFirstID < 10000)
                        //{
                        //    ID = OldLastID + 10000;
                        //}

                        //DB新增客戶部門歸屬清單
                        foreach (var CDA in CDAUpload)
                        {
                            CustomerDepartmentAttribution log = new CustomerDepartmentAttribution();
                            log.ID = ID;
                            log.GROUPID = CDA.GROUPID.Trim();
                            log.CUSTOMERNAME = CDA.CUSTOMERNAME.Trim();
                            log.CUSTOMERID = CDA.CUSTOMERID.Trim();
                            log.GROUPCODE = CDA.GROUPCODE.Trim();
                            log.GROUPTYPE = CDA.GROUPTYPE.Trim();
                            log.GROUPNAME = CDA.GROUPNAME.Trim();
                            log.DEPARTMENTID = CDA.DEPARTMENTID.Trim();
                            log.MANAGEREMAIL = CDA.MANAGEREMAIL.Trim();
                            log.MANAGERNAME = CDA.MANAGERNAME.Trim();
                            log.CREATE_TIME = System.DateTime.Now;
                            log.CREATE_USER = "System";
                            TecoApp.CustomerDepartmentAttribution.Add(log);
                            ID++;
                        }

                        TecoApp.SaveChanges();

                        logger.Debug("/CustomerDepartmentAttribution/Import - AddRangeSuccess");
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("/CDA/Import - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return await Query();
        }

        #endregion 匯入客戶部門歸屬資料

        #region 查詢客戶部門歸屬資料

        [HttpGet]
        [CorsHandle]
        public async Task<List<CDAResponseModel>> Query()
        {
            var CDAResponse = new List<CDAResponseModel>();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var CDAList = await TecoApp.CustomerDepartmentAttribution.AsNoTracking().ToListAsync();

                foreach (var CDA in CDAList)
                {
                    CDAResponse.Add(new CDAResponseModel() { 
                        ID = CDA.ID,
                        GROUPID = CDA.GROUPID,
                        CUSTOMERNAME = CDA.CUSTOMERNAME,
                        CUSTOMERID = CDA.CUSTOMERID,
                        GROUPCODE = CDA.GROUPCODE,
                        GROUPTYPE = CDA.GROUPTYPE,
                        GROUPNAME = CDA.GROUPNAME,
                        DEPARTMENTID = CDA.DEPARTMENTID,
                        MANAGEREMAIL = CDA.MANAGEREMAIL,
                        MANAGERNAME = CDA.MANAGERNAME
                    });
                }
            }
            catch (Exception ex)
            {
                logger.Error("/CDA/Query - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return CDAResponse;
        }

        #endregion 查詢客戶部門歸屬資料

        #endregion 後臺
    }
}
