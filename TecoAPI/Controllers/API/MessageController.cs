﻿using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using TecoModels;

namespace TecoAPI.Controllers
{
    public class MessageController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        [HttpPost]
        [CorsHandle]
        public Object Get([FromBody] JObject DATA)
        {
            MessageResponseModel MessageResponse = new MessageResponseModel();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var userId = DATA["ID"].ToString();
                var nowTime = DateTime.Now.AddMonths(-3);

                //撈自己三個月內的訂單訊息
                List<OrderNotification> MessageList = TecoApp.OrderNotification.Where(a => a.Tmcode == userId && DateTime.Compare(nowTime, a.CreateTime) < 0)
                                                            .OrderByDescending(b => b.CreateTime)
                                                            .AsEnumerable().ToList();
                foreach (var MessageItem in MessageList)
                {
                    MessageResponse.data.Add(new MessageModel()
                    {
                        messageId = "order_" + MessageItem.Id,
                        message = MessageItem.Message,
                        title = MessageItem.Title,
                        createTime = MessageItem.CreateTime,
                        tmCode = MessageItem.Tmcode,
                    });
                }

                MessageResponse.result = "1";
                MessageResponse.message = "Success";
            }
            catch (Exception ex)
            {
                logger.Error("/Message/Get - " + ex.Message);
                MessageResponse.result = "0";
                MessageResponse.message = "Exception:/Message/Get:" + ex.Message;
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return MessageResponse;
        }
    }
}
