﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using TecoAPI.Services;
using TecoModels;

namespace TecoAPI.Controllers
{
    [EnableCors("*", "*", "*")]
    public class ManagerController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region 後臺

        #region 查詢管理員資料

        [HttpGet]
        [CorsHandle]
        public async Task<List<MQueryResponseModel>> Query()
        {
            var MResponse = new List<MQueryResponseModel>();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var MList = await TecoApp.Manager.AsNoTracking().ToListAsync();

                foreach (var M in MList)
                {
                    MResponse.Add(new MQueryResponseModel()
                    {
                        ID = M.ID,
                        Account = M.Account,
                        Password = M.Password,
                        Name = M.Name,
                        Description = (string.IsNullOrWhiteSpace(M.Description) ? "" : M.Description),
                        Enable = M.Enable
                    });
                }
            }
            catch (Exception ex)
            {
                logger.Error("/Manager/Query - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return MResponse;
        }

        #endregion 查詢管理員資料

        #region 新增管理員資料

        [HttpPost]
        [CorsHandle]
        public async Task<List<MQueryResponseModel>> Add()
        {
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var Config = await TecoApp.Config.Where(p => p.Code == "RootFilePath" && p.Enable == "Y").AsNoTracking().ToListAsync();
                var RootFilePath = Config.FirstOrDefault().Value + "M\\";

                if (!Request.Content.IsMimeMultipartContent())
                {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                }

                if (!Directory.Exists(RootFilePath))
                {
                    Directory.CreateDirectory(RootFilePath);
                }

                var StreamProvider = new CustomMultipartFormDataStreamProvider(RootFilePath);

                await Request.Content.ReadAsMultipartAsync(StreamProvider);

                var MAddRequest = new MAddRequestModel();

                foreach (var Key in StreamProvider.FormData.AllKeys)
                {
                    foreach (var Value in StreamProvider.FormData.GetValues(Key))
                    {
                        MAddRequest = JsonConvert.DeserializeObject<MAddRequestModel>(Value);
                    }
                }

                Manager log = new Manager() { 
                    Account = MAddRequest.Account.Trim(),
                    Password = MAddRequest.Password.Trim(),
                    Name = MAddRequest.Name.Trim(),
                    Description = MAddRequest.Description.Trim(),
                    Enable = MAddRequest.Enable.Trim(),
                    CREATE_TIME = System.DateTime.Now,
                    CREATE_USER = "System"
                };
                TecoApp.Manager.Add(log);

                await TecoApp.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                logger.Error("/Manager/Add - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return await Query();
        }

        #endregion 新增管理員資料

        #region 修改管理員資料

        [HttpPut]
        [CorsHandle]
        public async Task<List<MQueryResponseModel>> Modify()
        {
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var Config = await TecoApp.Config.Where(p => p.Code == "RootFilePath" && p.Enable == "Y").AsNoTracking().ToListAsync();
                var RootFilePath = Config.FirstOrDefault().Value + "M\\";

                if (!Request.Content.IsMimeMultipartContent())
                {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                }

                if (!Directory.Exists(RootFilePath))
                {
                    Directory.CreateDirectory(RootFilePath);
                }

                var StreamProvider = new CustomMultipartFormDataStreamProvider(RootFilePath);

                await Request.Content.ReadAsMultipartAsync(StreamProvider);

                var MModifyRequest = new MModifyRequestModel();

                foreach (var Key in StreamProvider.FormData.AllKeys)
                {
                    foreach (var Value in StreamProvider.FormData.GetValues(Key))
                    {
                        MModifyRequest = JsonConvert.DeserializeObject<MModifyRequestModel>(Value);
                    }
                }

                var Manager = await TecoApp.Manager.Where(x => x.ID == MModifyRequest.ID).FirstOrDefaultAsync();
                Manager.Account = MModifyRequest.Account.Trim();
                Manager.Password = MModifyRequest.Password.Trim();
                Manager.Name = MModifyRequest.Name.Trim();
                Manager.Description = MModifyRequest.Description.Trim();
                Manager.Enable = MModifyRequest.Enable.Trim();
                Manager.UPDATE_TIME = System.DateTime.Now;
                Manager.UPDATE_USER = "System";
                TecoApp.Entry(Manager).State = EntityState.Modified;

                await TecoApp.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                logger.Error("/Giveaway/Modify - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return await Query();
        }

        #endregion 修改管理員資料

        #region 刪除管理員資料

        [HttpDelete]
        [CorsHandle]
        public async Task<List<MQueryResponseModel>> Delete(int id)
        {
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var Manager = await TecoApp.Manager.Where(x => x.ID == id).FirstOrDefaultAsync();
                TecoApp.Manager.Remove(Manager);
                TecoApp.Entry(Manager).State = EntityState.Deleted;

                await TecoApp.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                logger.Error("/Manager/Delete - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return await Query();
        }

        #endregion 刪除管理員資料

        #endregion 後臺
    }
}
