﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using System.Web.Http;
using System.Web.UI.WebControls;
using TecoAPI.Services;
using TecoModels;
using TecoModels.NotificationModel;
using TecoModels.NotificationModel.RequestBody;
using TecoModels.NotificationModel.ResponseBody;

namespace TecoAPI.Controllers
{
    public class OrderController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region 前臺

        #region 新增訂單

        [HttpPost]
        [CorsHandle]
        public async Task<Object> Add([FromBody] JObject DATA)
        {
            ResultModel OrderResponse = new ResultModel();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                OrderRequestModel OrderRequest = JsonConvert.DeserializeObject<OrderRequestModel>(JsonConvert.SerializeObject(DATA));

                var Config = await TecoApp.Config.Where(p => p.Code == "ServiceURL" && p.Enable == "Y").AsNoTracking().ToListAsync();
                var ServiceURL = Config.FirstOrDefault().Value;
                var KNA1List = await TecoApp.KNA1.Where(p => p.KUNNR == OrderRequest.ID).AsNoTracking().ToListAsync();

                //判斷驗證是否成功
                if (KNA1List.Count() > 0)
                {
                    ProductRRequestModel ProductRRequest = new ProductRRequestModel() { 
                        ID = OrderRequest.ID,
                        Type = "ALL"
                    };

                    //取得商品資料
                    var GetData = HttpClientHelper.POST_JSON(ServiceURL + "/API/ProductR/Get", JsonConvert.SerializeObject(ProductRRequest));
                    ProductRResponseModel ProductRResponse = JsonConvert.DeserializeObject<ProductRResponseModel>(HttpClientHelper.ConvertString(GetData));

                    #region 設定隨機訂單編號

                    //var OrderCode = GetRandomCode_EnglishAndNumber(10);

                    ////查詢訂單號碼有無重複
                    //var OrderList = await TecoApp.Order.Where(p => p.CODE == OrderCode).AsNoTracking().ToListAsync();
                    //while (OrderList.Count() != 0)
                    //{
                    //    OrderCode = GetRandomCode_EnglishAndNumber(10);
                    //    OrderList = await TecoApp.Order.Where(p => p.CODE == OrderCode).AsNoTracking().ToListAsync();
                    //}

                    #endregion 設定隨機訂單編號

                    #region 設定訂單編號(10碼:yyMMddxxxx)

                    //起訖時間
                    var DT_Now = DateTime.Now;
                    var Str_Now = DT_Now.ToString("yyyy-MM-dd");
                    var DT_Start = DateTime.Parse(Str_Now);
                    var DT_End = DT_Start.AddDays(1);

                    //查詢訂單號碼
                    var OrderList = await TecoApp.Order.Where(p => p.CREATE_TIME >= DT_Start && p.CREATE_TIME < DT_End).OrderByDescending(p => p.CREATE_TIME).AsNoTracking().ToListAsync();

                    string OrderCode = string.Empty;
                    if (OrderList == null)
                    {
                        OrderCode = GetNewOrderID(DT_Now.ToString("yyMMdd"), "");
                    }
                    else
                    {
                        OrderCode = GetNewOrderID(DT_Now.ToString("yyMMdd"), OrderList.FirstOrDefault().CODE);
                    }

                    #endregion 設定訂單編號(10碼:yyMMddxxxx)

                    //設定訂單主檔
                    Order OrderLog = new Order();
                    OrderLog.CODE = OrderCode;
                    OrderLog.TYPE = OrderRequest.TYPE;
                    OrderLog.DEPARTMENT = OrderRequest.DEPARTMENT;
                    OrderLog.CUSTOMER = OrderRequest.ID;
                    OrderLog.PURCHASEORDERCODE = OrderRequest.PURCHASEORDERCODE;
                    OrderLog.CITY = OrderRequest.CITY;
                    OrderLog.ADDRESS = OrderRequest.ADDRESS;
                    OrderLog.PHONE = OrderRequest.PHONE;
                    OrderLog.MOBILE = OrderRequest.MOBILE;
                    OrderLog.RECEIVER = OrderRequest.RECEIVER;
                    OrderLog.DELIVERYINSTRUCTIONCODE = OrderRequest.DELIVERYINSTRUCTIONCODE;
                    OrderLog.NOTE = OrderRequest.NOTE;
                    OrderLog.SHIPPINGTIME = OrderRequest.SHIPPINGTIME;
                    OrderLog.LOCKED = "N";
                    OrderLog.BATCH = "N";
                    OrderLog.STATUS = "1";
                    OrderLog.CREATE_TIME = System.DateTime.Now;
                    OrderLog.CREATE_USER = "System";
                    TecoApp.Order.Add(OrderLog);

                    //設定訂單副檔
                    foreach (var OrderItem in OrderRequest.data)
                    {
                        #region 設定訂單價格

                        var ProductPrice = "";
                        var RelatedProductPrice = "";

                        foreach (var ProductR in ProductRResponse.data) 
                        {
                            if (ProductR.PRODUCTCODE == OrderItem.PRODUCTCODE) 
                            {
                                foreach (var PRODUCTPRICE in ProductR.PRODUCTPRICE) 
                                {
                                    if (PRODUCTPRICE.TYPE == OrderItem.PRICETYPE) 
                                    {
                                        ProductPrice = PRODUCTPRICE.PRICE;
                                        break;
                                    }
                                }
                            }

                            if (OrderItem.RELATEDPRODUCTCODE != "" && OrderItem.RELATEDPRODUCTQUANTITY != "")
                            {
                                if (ProductR.RELATEDPRODUCTCODE == OrderItem.RELATEDPRODUCTCODE)
                                {
                                    foreach (var RELATEDPRODUCTPRICE in ProductR.RELATEDPRODUCTPRICE)
                                    {
                                        if (RELATEDPRODUCTPRICE.TYPE == OrderItem.PRICETYPE)
                                        {
                                            RelatedProductPrice = RELATEDPRODUCTPRICE.PRICE;
                                            break;
                                        }
                                    }
                                }

                                if (RelatedProductPrice == "") 
                                {
                                    if (ProductR.PRODUCTCODE == OrderItem.RELATEDPRODUCTCODE)
                                    {
                                        foreach (var PRODUCTPRICE in ProductR.PRODUCTPRICE)
                                        {
                                            if (PRODUCTPRICE.TYPE == OrderItem.PRICETYPE)
                                            {
                                                RelatedProductPrice = PRODUCTPRICE.PRICE;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }

                            if (OrderItem.RELATEDPRODUCTCODE != "" && OrderItem.RELATEDPRODUCTQUANTITY != "")
                            {
                                if (ProductPrice != "" && RelatedProductPrice != "")
                                {
                                    break;
                                }
                            }
                            else 
                            {
                                if (ProductPrice != "")
                                {
                                    break;
                                }
                            }
                        }

                        #endregion 設定訂單價格

                        #region  設定訂單明細商品

                        //設定訂單主商品
                        OrderItem OrderItemLog = new OrderItem();
                        OrderItemLog.ORDERCODE = OrderCode;
                        OrderItemLog.PRICE = ProductPrice;
                        OrderItemLog.CODE = "10";
                        OrderItemLog.ATTRIBUTIONTYPE = "1";
                        OrderItemLog.MAINPRODUCTCODE = OrderItem.PRODUCTCODE;
                        OrderItemLog.PRODUCTCODE = OrderItem.PRODUCTCODE;
                        OrderItemLog.GROUPTYPE = "R000";
                        OrderItemLog.PRICETYPE = OrderItem.PRICETYPE;
                        OrderItemLog.FACTORY = "15R0";
                        OrderItemLog.WAREHOUSE = OrderRequest.WAREHOUSE;
                        OrderItemLog.QUANTITY = OrderItem.PRODUCTQUANTITY;
                        OrderItemLog.SHIPPINGTIME = "";
                        OrderItemLog.INVENTORY = "";
                        OrderItemLog.SALESCODE = "";
                        OrderItemLog.DELIVERYCODE = "";
                        OrderItemLog.POSTINGCODE = "";
                        OrderItemLog.INVOICECODE = "";
                        OrderItemLog.ERRORTYPE = "";
                        OrderItemLog.ERRORMESSAGE = "";
                        OrderItemLog.ERRORDATE = "";
                        OrderItemLog.ERRORTIME = "";
                        OrderItemLog.CREATE_TIME = System.DateTime.Now;
                        OrderItemLog.CREATE_USER = "System";
                        TecoApp.OrderItem.Add(OrderItemLog);

                        //設定訂單關聯商品
                        if (OrderItem.RELATEDPRODUCTCODE != "" && OrderItem.RELATEDPRODUCTQUANTITY != "" && OrderItem.RELATEDPRODUCTCODE.Replace("-", "").Length > 4)
                        {
                            OrderItem RelatedOrderItemLog = new OrderItem();
                            RelatedOrderItemLog.ORDERCODE = OrderCode;
                            RelatedOrderItemLog.PRICE = RelatedProductPrice;
                            RelatedOrderItemLog.CODE = "20";
                            RelatedOrderItemLog.ATTRIBUTIONTYPE = "2";
                            RelatedOrderItemLog.MAINPRODUCTCODE = OrderItem.PRODUCTCODE;
                            RelatedOrderItemLog.PRODUCTCODE = OrderItem.RELATEDPRODUCTCODE;
                            RelatedOrderItemLog.GROUPTYPE = "R000";
                            RelatedOrderItemLog.PRICETYPE = OrderItem.PRICETYPE;
                            RelatedOrderItemLog.FACTORY = "15R0";
                            RelatedOrderItemLog.WAREHOUSE = OrderRequest.WAREHOUSE;
                            RelatedOrderItemLog.QUANTITY = OrderItem.RELATEDPRODUCTQUANTITY;
                            RelatedOrderItemLog.SHIPPINGTIME = "";
                            RelatedOrderItemLog.INVENTORY = "";
                            RelatedOrderItemLog.SALESCODE = "";
                            RelatedOrderItemLog.DELIVERYCODE = "";
                            RelatedOrderItemLog.POSTINGCODE = "";
                            RelatedOrderItemLog.INVOICECODE = "";
                            RelatedOrderItemLog.ERRORTYPE = "";
                            RelatedOrderItemLog.ERRORMESSAGE = "";
                            RelatedOrderItemLog.ERRORDATE = "";
                            RelatedOrderItemLog.ERRORTIME = "";
                            RelatedOrderItemLog.CREATE_TIME = System.DateTime.Now;
                            RelatedOrderItemLog.CREATE_USER = "System";
                            TecoApp.OrderItem.Add(RelatedOrderItemLog);
                        }

                        //設定訂單贈品
                        if (OrderItem.Giveaway != null) 
                        {
                            foreach (var GiveawayItem in OrderItem.Giveaway)
                            {
                                if (GiveawayItem.PRODUCTCODE != "" && GiveawayItem.PRODUCTQUANTITY != "")
                                {
                                    OrderItem GiveawayItemLog = new OrderItem();
                                    GiveawayItemLog.ORDERCODE = OrderCode;
                                    GiveawayItemLog.PRICE = "0";
                                    GiveawayItemLog.CODE = "10";
                                    GiveawayItemLog.ATTRIBUTIONTYPE = "3";
                                    GiveawayItemLog.MAINPRODUCTCODE = OrderItem.PRODUCTCODE;
                                    GiveawayItemLog.PRODUCTCODE = GiveawayItem.PRODUCTCODE;
                                    GiveawayItemLog.GROUPTYPE = "K000";
                                    GiveawayItemLog.PRICETYPE = "28";
                                    GiveawayItemLog.FACTORY = "12K0";
                                    GiveawayItemLog.WAREHOUSE = OrderRequest.WAREHOUSE;
                                    GiveawayItemLog.QUANTITY = GiveawayItem.PRODUCTQUANTITY;
                                    GiveawayItemLog.SHIPPINGTIME = "";
                                    GiveawayItemLog.INVENTORY = "";
                                    GiveawayItemLog.SALESCODE = "";
                                    GiveawayItemLog.DELIVERYCODE = "";
                                    GiveawayItemLog.POSTINGCODE = "";
                                    GiveawayItemLog.INVOICECODE = "";
                                    GiveawayItemLog.ERRORTYPE = "";
                                    GiveawayItemLog.ERRORMESSAGE = "";
                                    GiveawayItemLog.ERRORDATE = "";
                                    GiveawayItemLog.ERRORTIME = "";
                                    GiveawayItemLog.CREATE_TIME = System.DateTime.Now;
                                    GiveawayItemLog.CREATE_USER = "System";
                                    TecoApp.OrderItem.Add(GiveawayItemLog);
                                }
                            }
                        }

                        #endregion 設定訂單明細商品
                    }
                    await TecoApp.SaveChangesAsync();

                    OrderResponse.result = "1";
                    OrderResponse.message = "Success";
                }
                else
                {
                    OrderResponse.result = "0";
                    OrderResponse.message = "Error:/Order/Add:ID檢核不符";
                }
            }
            catch (Exception ex)
            {
                logger.Error("/Order/Add - " + ex.Message);
                OrderResponse.result = "0";
                OrderResponse.message = "Exception:/Order/Add:" + ex.Message;
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return OrderResponse;
        }

        public string GetNewOrderID(string Str_Now, string LastID)
        {
            string result = Str_Now;

            if (string.IsNullOrWhiteSpace(LastID) || !LastID.Contains(Str_Now))
            {
                result += "0001";
            }
            else
            {
                var newStr_LastID = LastID.Replace(Str_Now, "");
                //16進位
                //var new16_LastID = Convert.ToString(Convert.ToInt32(newStr_LastID, 16) + Convert.ToInt32("0001", 16), 16);
                //result += new16_LastID.PadLeft(4, '0');
                //10進位
                var new10_LastID = Convert.ToInt32(newStr_LastID) + 1;
                result += new10_LastID.ToString().PadLeft(4, '0');
            }

            return result;
        }

        public string GetRandomCode_EnglishAndNumber(int No)
        {
            string result = "";
            Random RandomObj = new Random();
            string[] RandomString = new string[] 
            { 
                "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
                "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
                "U", "V", "W", "X", "Y", "Z",
                "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"
            };

            for (int i = 0; i < No; i++)
            {
                result += RandomString[RandomObj.Next(RandomString.Count())];
            }

            return result;
        }

        #endregion 新增訂單

        #region 鎖定訂單

        [HttpPost]
        [CorsHandle]
        public async Task<Object> Lock([FromBody] JObject DATA)
        {
            ResultModel OrderLockResponse = new ResultModel();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                OrderLockRequestModel OrderLockRequest = JsonConvert.DeserializeObject<OrderLockRequestModel>(JsonConvert.SerializeObject(DATA));

                var KNA1List = await TecoApp.KNA1.Where(p => p.KUNNR == OrderLockRequest.ID).AsNoTracking().ToListAsync();

                //判斷驗證是否成功
                if (KNA1List.Count() > 0)
                {
                    var OrderList = await TecoApp.Order.Where(p => p.CODE == OrderLockRequest.CODE).AsNoTracking().ToListAsync();
                    var OrderItem = OrderList.FirstOrDefault();

                    //判斷未批次+未送出狀態
                    if (OrderItem.BATCH == "N" && OrderItem.STATUS == "1")
                    {
                        //設定為已鎖定+修改中狀態
                        OrderItem.LOCKED = "Y";
                        OrderItem.STATUS = "2";
                        OrderItem.UPDATE_TIME = System.DateTime.Now;
                        OrderItem.UPDATE_USER = "System";
                        TecoApp.Entry(OrderItem).State = EntityState.Modified;
                        await TecoApp.SaveChangesAsync();

                        OrderLockResponse.result = "1";
                        OrderLockResponse.message = "Success";
                    }
                    else
                    {
                        OrderLockResponse.result = "0";
                        OrderLockResponse.message = "Error:/Order/Lock:訂單不可修改";
                    }
                }
                else
                {
                    OrderLockResponse.result = "0";
                    OrderLockResponse.message = "Error:/Order/Lock:ID檢核不符";
                }
            }
            catch (Exception ex)
            {
                logger.Error("/Order/Lock - " + ex.Message);
                OrderLockResponse.result = "0";
                OrderLockResponse.message = "Exception:/Order/Lock:" + ex.Message;
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return OrderLockResponse;
        }

        #endregion 鎖定訂單

        #region 修改訂單

        [HttpPost]
        [CorsHandle]
        public async Task<Object> Modify([FromBody] JObject DATA)
        {
            ResultModel OrderResponse = new ResultModel();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                OrderModifyRequestModel OrderModifyRequest = JsonConvert.DeserializeObject<OrderModifyRequestModel>(JsonConvert.SerializeObject(DATA));

                var Config = await TecoApp.Config.Where(p => p.Code == "ServiceURL" && p.Enable == "Y").AsNoTracking().ToListAsync();
                var ServiceURL = Config.FirstOrDefault().Value;
                var KNA1List = await TecoApp.KNA1.Where(p => p.KUNNR == OrderModifyRequest.ID).AsNoTracking().ToListAsync();

                //判斷驗證是否成功
                if (KNA1List.Count() > 0)
                {
                    var OrderList = await TecoApp.Order.Where(p => p.CUSTOMER == OrderModifyRequest.ID && p.CODE == OrderModifyRequest.CODE).AsNoTracking().ToListAsync();

                    //判斷訂單存在
                    if (OrderList.Count() > 0)
                    {
                        var OrderTarget = OrderList.FirstOrDefault();

                        //判斷已鎖定+未批次+修改中狀態
                        if (OrderTarget.LOCKED == "Y" && OrderTarget.BATCH == "N" && OrderTarget.STATUS == "2")
                        {
                            ProductRRequestModel ProductRRequest = new ProductRRequestModel()
                            {
                                ID = OrderModifyRequest.ID,
                                Type = "ALL"
                            };

                            //取得商品資料
                            var GetData = HttpClientHelper.POST_JSON(ServiceURL + "/API/ProductR/Get", JsonConvert.SerializeObject(ProductRRequest));
                            ProductRResponseModel ProductRResponse = JsonConvert.DeserializeObject<ProductRResponseModel>(HttpClientHelper.ConvertString(GetData));

                            var OrderCode = OrderModifyRequest.CODE;

                            //更新訂單主檔
                            OrderTarget.TYPE = OrderModifyRequest.TYPE;
                            OrderTarget.PURCHASEORDERCODE = OrderModifyRequest.PURCHASEORDERCODE;
                            OrderTarget.CITY = OrderModifyRequest.CITY;
                            OrderTarget.ADDRESS = OrderModifyRequest.ADDRESS;
                            OrderTarget.PHONE = OrderModifyRequest.PHONE;
                            OrderTarget.MOBILE = OrderModifyRequest.MOBILE;
                            OrderTarget.RECEIVER = OrderModifyRequest.RECEIVER;
                            OrderTarget.DELIVERYINSTRUCTIONCODE = OrderModifyRequest.DELIVERYINSTRUCTIONCODE;
                            OrderTarget.NOTE = OrderModifyRequest.NOTE;
                            OrderTarget.SHIPPINGTIME = OrderModifyRequest.SHIPPINGTIME;
                            OrderTarget.LOCKED = "N";
                            OrderTarget.BATCH = "N";
                            OrderTarget.STATUS = "1";
                            OrderTarget.CREATE_TIME = System.DateTime.Now;
                            TecoApp.Entry(OrderTarget).State = EntityState.Modified;

                            //清除訂單副檔
                            var OrderItemList = await TecoApp.OrderItem.Where(p => p.ORDERCODE == OrderCode).AsNoTracking().ToListAsync();
                            TecoApp.OrderItem.RemoveRange(OrderItemList);

                            foreach (var OrderItem in OrderModifyRequest.data)
                            {
                                #region 設定訂單價格

                                var ProductPrice = "";
                                var RelatedProductPrice = "";

                                foreach (var ProductR in ProductRResponse.data)
                                {
                                    if (ProductR.PRODUCTCODE == OrderItem.PRODUCTCODE)
                                    {
                                        foreach (var PRODUCTPRICE in ProductR.PRODUCTPRICE)
                                        {
                                            if (PRODUCTPRICE.TYPE == OrderItem.PRICETYPE)
                                            {
                                                ProductPrice = PRODUCTPRICE.PRICE;
                                                break;
                                            }
                                        }
                                    }

                                    if (OrderItem.RELATEDPRODUCTCODE != "" && OrderItem.RELATEDPRODUCTQUANTITY != "")
                                    {
                                        if (ProductR.RELATEDPRODUCTCODE == OrderItem.RELATEDPRODUCTCODE)
                                        {
                                            foreach (var RELATEDPRODUCTPRICE in ProductR.RELATEDPRODUCTPRICE)
                                            {
                                                if (RELATEDPRODUCTPRICE.TYPE == OrderItem.PRICETYPE)
                                                {
                                                    RelatedProductPrice = RELATEDPRODUCTPRICE.PRICE;
                                                    break;
                                                }
                                            }
                                        }

                                        if (RelatedProductPrice == "")
                                        {
                                            if (ProductR.PRODUCTCODE == OrderItem.RELATEDPRODUCTCODE)
                                            {
                                                foreach (var PRODUCTPRICE in ProductR.PRODUCTPRICE)
                                                {
                                                    if (PRODUCTPRICE.TYPE == OrderItem.PRICETYPE)
                                                    {
                                                        RelatedProductPrice = PRODUCTPRICE.PRICE;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if (OrderItem.RELATEDPRODUCTCODE != "" && OrderItem.RELATEDPRODUCTQUANTITY != "")
                                    {
                                        if (ProductPrice != "" && RelatedProductPrice != "")
                                        {
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if (ProductPrice != "")
                                        {
                                            break;
                                        }
                                    }
                                }

                                #endregion 設定訂單價格

                                #region 設定訂單明細商品

                                //設定訂單主商品
                                OrderItem OrderItemLog = new OrderItem();
                                OrderItemLog.ORDERCODE = OrderCode;
                                OrderItemLog.PRICE = ProductPrice;
                                OrderItemLog.CODE = "10";
                                OrderItemLog.ATTRIBUTIONTYPE = "1";
                                OrderItemLog.MAINPRODUCTCODE = OrderItem.PRODUCTCODE;
                                OrderItemLog.PRODUCTCODE = OrderItem.PRODUCTCODE;
                                OrderItemLog.GROUPTYPE = "R000";
                                OrderItemLog.PRICETYPE = OrderItem.PRICETYPE;
                                OrderItemLog.FACTORY = "15R0";
                                OrderItemLog.WAREHOUSE = OrderModifyRequest.WAREHOUSE;
                                OrderItemLog.QUANTITY = OrderItem.PRODUCTQUANTITY;
                                OrderItemLog.SHIPPINGTIME = "";
                                OrderItemLog.INVENTORY = "";
                                OrderItemLog.SALESCODE = "";
                                OrderItemLog.DELIVERYCODE = "";
                                OrderItemLog.POSTINGCODE = "";
                                OrderItemLog.INVOICECODE = "";
                                OrderItemLog.ERRORTYPE = "";
                                OrderItemLog.ERRORMESSAGE = "";
                                OrderItemLog.ERRORDATE = "";
                                OrderItemLog.ERRORTIME = "";
                                OrderItemLog.CREATE_TIME = System.DateTime.Now;
                                OrderItemLog.CREATE_USER = "System";
                                TecoApp.OrderItem.Add(OrderItemLog);

                                //設定訂單關聯商品
                                if (OrderItem.RELATEDPRODUCTCODE != "" && OrderItem.RELATEDPRODUCTQUANTITY != "")
                                {
                                    OrderItem RelatedOrderItemLog = new OrderItem();
                                    RelatedOrderItemLog.ORDERCODE = OrderCode;
                                    RelatedOrderItemLog.PRICE = RelatedProductPrice;
                                    RelatedOrderItemLog.CODE = "20";
                                    RelatedOrderItemLog.ATTRIBUTIONTYPE = "2";
                                    RelatedOrderItemLog.MAINPRODUCTCODE = OrderItem.PRODUCTCODE;
                                    RelatedOrderItemLog.PRODUCTCODE = OrderItem.RELATEDPRODUCTCODE;
                                    RelatedOrderItemLog.GROUPTYPE = "R000";
                                    RelatedOrderItemLog.PRICETYPE = OrderItem.PRICETYPE;
                                    RelatedOrderItemLog.FACTORY = "15R0";
                                    RelatedOrderItemLog.WAREHOUSE = OrderModifyRequest.WAREHOUSE;
                                    RelatedOrderItemLog.QUANTITY = OrderItem.RELATEDPRODUCTQUANTITY;
                                    RelatedOrderItemLog.SHIPPINGTIME = "";
                                    RelatedOrderItemLog.INVENTORY = "";
                                    RelatedOrderItemLog.SALESCODE = "";
                                    RelatedOrderItemLog.DELIVERYCODE = "";
                                    RelatedOrderItemLog.POSTINGCODE = "";
                                    RelatedOrderItemLog.INVOICECODE = "";
                                    RelatedOrderItemLog.ERRORTYPE = "";
                                    RelatedOrderItemLog.ERRORMESSAGE = "";
                                    RelatedOrderItemLog.ERRORDATE = "";
                                    RelatedOrderItemLog.ERRORTIME = "";
                                    RelatedOrderItemLog.CREATE_TIME = System.DateTime.Now;
                                    RelatedOrderItemLog.CREATE_USER = "System";
                                    TecoApp.OrderItem.Add(RelatedOrderItemLog);
                                }

                                //設定訂單贈品
                                if (OrderItem.Giveaway != null)
                                {
                                    foreach (var GiveawayItem in OrderItem.Giveaway)
                                    {
                                        if (GiveawayItem.PRODUCTCODE != "" && GiveawayItem.PRODUCTQUANTITY != "") 
                                        {
                                            OrderItem GiveawayItemLog = new OrderItem();
                                            GiveawayItemLog.ORDERCODE = OrderCode;
                                            GiveawayItemLog.PRICE = "0";
                                            GiveawayItemLog.CODE = "10";
                                            GiveawayItemLog.ATTRIBUTIONTYPE = "3";
                                            GiveawayItemLog.MAINPRODUCTCODE = OrderItem.PRODUCTCODE;
                                            GiveawayItemLog.PRODUCTCODE = GiveawayItem.PRODUCTCODE;
                                            GiveawayItemLog.GROUPTYPE = "K000";
                                            GiveawayItemLog.PRICETYPE = "28";
                                            GiveawayItemLog.FACTORY = "12K0";
                                            GiveawayItemLog.WAREHOUSE = OrderModifyRequest.WAREHOUSE;
                                            GiveawayItemLog.QUANTITY = GiveawayItem.PRODUCTQUANTITY;
                                            GiveawayItemLog.SHIPPINGTIME = "";
                                            GiveawayItemLog.INVENTORY = "";
                                            GiveawayItemLog.SALESCODE = "";
                                            GiveawayItemLog.DELIVERYCODE = "";
                                            GiveawayItemLog.POSTINGCODE = "";
                                            GiveawayItemLog.INVOICECODE = "";
                                            GiveawayItemLog.ERRORTYPE = "";
                                            GiveawayItemLog.ERRORMESSAGE = "";
                                            GiveawayItemLog.ERRORDATE = "";
                                            GiveawayItemLog.ERRORTIME = "";
                                            GiveawayItemLog.CREATE_TIME = System.DateTime.Now;
                                            GiveawayItemLog.CREATE_USER = "System";
                                            TecoApp.OrderItem.Add(GiveawayItemLog);
                                        }
                                    }
                                }

                                #endregion 設定訂單明細商品
                            }
                            await TecoApp.SaveChangesAsync();

                            OrderResponse.result = "1";
                            OrderResponse.message = "Success";
                        }
                        else 
                        {
                            OrderResponse.result = "0";
                            OrderResponse.message = "Error:/Order/Modify:訂單不可修改";
                        }
                    }
                    else
                    {
                        OrderResponse.result = "0";
                        OrderResponse.message = "Error:/Order/Modify:無此訂單號碼";
                    }
                }
                else
                {
                    OrderResponse.result = "0";
                    OrderResponse.message = "Error:/Order/Modify:ID檢核不符";
                }
            }
            catch (Exception ex)
            {
                logger.Error("/Order/Modify - " + ex.Message);
                OrderResponse.result = "0";
                OrderResponse.message = "Exception:/Order/Modify:" + ex.Message;
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return OrderResponse;
        }

        #endregion 修改訂單

        #region 刪除訂單

        [HttpPost]
        [CorsHandle]
        public async Task<Object> Delete([FromBody] JObject DATA)
        {
            ResultModel OrderDeleteResponse = new ResultModel();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                OrderDeleteRequestModel OrderDeleteRequest = JsonConvert.DeserializeObject<OrderDeleteRequestModel>(JsonConvert.SerializeObject(DATA));

                var KNA1List = await TecoApp.KNA1.Where(p => p.KUNNR == OrderDeleteRequest.ID).AsNoTracking().ToListAsync();

                //判斷驗證是否成功
                if (KNA1List.Count() > 0)
                {
                    var OrderList = await TecoApp.Order.Where(p => p.CODE == OrderDeleteRequest.CODE).AsNoTracking().ToListAsync();
                    var OrderItem = OrderList.FirstOrDefault();

                    //判斷未批次+(狀態:已刪除/未送出/修改中)
                    if (OrderItem.BATCH == "N" && (OrderItem.STATUS == "0" || OrderItem.STATUS == "1" || OrderItem.STATUS == "2"))
                    {
                        OrderItem.STATUS = "0";
                        OrderItem.UPDATE_TIME = System.DateTime.Now;
                        OrderItem.UPDATE_USER = "System";
                        TecoApp.Entry(OrderItem).State = EntityState.Modified;

                        await TecoApp.SaveChangesAsync();

                        OrderDeleteResponse.result = "1";
                        OrderDeleteResponse.message = "Success";
                    }
                    else
                    {
                        OrderDeleteResponse.result = "0";
                        OrderDeleteResponse.message = "Error:/Order/Delete:訂單刪除請聯絡客服";
                    }
                }
                else
                {
                    OrderDeleteResponse.result = "0";
                    OrderDeleteResponse.message = "Error:/Order/Delete:ID檢核不符";
                }
            }
            catch (Exception ex)
            {
                logger.Error("/Order/Delete - " + ex.Message);
                OrderDeleteResponse.result = "0";
                OrderDeleteResponse.message = "Exception:/Order/Delete:" + ex.Message;
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return OrderDeleteResponse;
        }

        #endregion 刪除訂單

        #region 查詢訂單

        [HttpPost]
        [CorsHandle]
        public async Task<Object> Query([FromBody] JObject DATA)
        {
            OrderQueryResponseModel OrderQueryResponse = new OrderQueryResponseModel();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                OrderQueryRequestModel OrderQueryRequest = JsonConvert.DeserializeObject<OrderQueryRequestModel>(JsonConvert.SerializeObject(DATA));

                var Config = await TecoApp.Config.Where(p => p.Code == "ServiceURL" && p.Enable == "Y").AsNoTracking().ToListAsync();
                var ServiceURL = Config.FirstOrDefault().Value;
                var KNA1List = await TecoApp.KNA1.Where(p => p.KUNNR == OrderQueryRequest.ID).AsNoTracking().ToListAsync();

                //判斷驗證是否成功
                if (KNA1List.Count() > 0)
                {
                    var OrderList = await TecoApp.Order.Where(p => p.CUSTOMER == OrderQueryRequest.ID).OrderByDescending(p => p.CREATE_TIME).AsNoTracking().ToListAsync();

                    foreach (var OrderItem in OrderList)
                    {
                        var ProductList = await TecoApp.OrderItem.Where(p => p.ORDERCODE == OrderItem.CODE).AsNoTracking().ToListAsync();
                        var WAREHOUSE = "";

                        List<OrderQueryProductModel> OrderQueryProductList = new List<OrderQueryProductModel>();
                        OrderQueryProductModel OrderQueryProduct = new OrderQueryProductModel();
                        List<GiveawayQueryItemModel> GiveawayQueryItem = new List<GiveawayQueryItemModel>();

                        #region 設定訂單副檔

                        var CheckFirst = true;
                        foreach (var ProductItem in ProductList) 
                        {
                            if (WAREHOUSE == "") 
                            {
                                WAREHOUSE = ProductItem.WAREHOUSE;
                            }

                            if (ProductItem.ATTRIBUTIONTYPE == "1") 
                            {
                                if (CheckFirst) 
                                {
                                    CheckFirst = false;
                                }
                                else
                                {
                                    OrderQueryProduct.Giveaway = GiveawayQueryItem;
                                    OrderQueryProductList.Add(OrderQueryProduct);
                                    OrderQueryProduct = new OrderQueryProductModel();
                                    GiveawayQueryItem = new List<GiveawayQueryItemModel>();
                                }

                                OrderQueryProduct.PRICETYPE = ProductItem.PRICETYPE;
                                OrderQueryProduct.PRODUCTCODE = ProductItem.PRODUCTCODE;
                                OrderQueryProduct.PRODUCTQUANTITY = ProductItem.QUANTITY;
                                OrderQueryProduct.SHIPPINGTIME = ProductItem.SHIPPINGTIME;
                                OrderQueryProduct.SALESCODE = ProductItem.SALESCODE;
                                OrderQueryProduct.DELIVERYCODE = ProductItem.DELIVERYCODE;
                                OrderQueryProduct.POSTINGCODE = ProductItem.POSTINGCODE;
                                OrderQueryProduct.INVOICECODE = ProductItem.INVOICECODE;
                                OrderQueryProduct.ERRORTYPE = ProductItem.ERRORTYPE;
                                OrderQueryProduct.ERRORMESSAGE = ProductItem.ERRORMESSAGE;
                                OrderQueryProduct.ERRORDATE = ProductItem.ERRORDATE;
                                OrderQueryProduct.ERRORTIME = ProductItem.ERRORTIME;
                            }
                            else if (ProductItem.ATTRIBUTIONTYPE == "2")
                            {
                                OrderQueryProduct.RELATEDPRODUCTCODE = ProductItem.PRODUCTCODE;
                                OrderQueryProduct.RELATEDPRODUCTQUANTITY = ProductItem.QUANTITY;
                                OrderQueryProduct.RELATEDSHIPPINGTIME = ProductItem.SHIPPINGTIME;
                                OrderQueryProduct.SALESCODE = ProductItem.SALESCODE;
                                OrderQueryProduct.DELIVERYCODE = ProductItem.DELIVERYCODE;
                                OrderQueryProduct.POSTINGCODE = ProductItem.POSTINGCODE;
                                OrderQueryProduct.INVOICECODE = ProductItem.INVOICECODE;
                                OrderQueryProduct.ERRORTYPE = ProductItem.ERRORTYPE;
                                OrderQueryProduct.ERRORMESSAGE = ProductItem.ERRORMESSAGE;
                                OrderQueryProduct.ERRORDATE = ProductItem.ERRORDATE;
                                OrderQueryProduct.ERRORTIME = ProductItem.ERRORTIME;
                            }
                            else if (ProductItem.ATTRIBUTIONTYPE == "3")
                            {
                                GiveawayQueryItem.Add(new GiveawayQueryItemModel() { 
                                    PRODUCTCODE = ProductItem.PRODUCTCODE,
                                    PRODUCTQUANTITY = ProductItem.QUANTITY,
                                    SHIPPINGTIME = ProductItem.SHIPPINGTIME,
                                    SALESCODE = ProductItem.SALESCODE,
                                    DELIVERYCODE = ProductItem.DELIVERYCODE,
                                    POSTINGCODE = ProductItem.POSTINGCODE,
                                    INVOICECODE = ProductItem.INVOICECODE,
                                    ERRORTYPE = ProductItem.ERRORTYPE,
                                    ERRORMESSAGE = ProductItem.ERRORMESSAGE,
                                    ERRORDATE = ProductItem.ERRORDATE,
                                    ERRORTIME = ProductItem.ERRORTIME
                                });
                            }

                            if (string.IsNullOrWhiteSpace(ProductItem.DELIVERYCODE))
                            {
                                DeliveryRequestModel DeliveryRequest = new DeliveryRequestModel();
                                DeliveryRequest.ID = OrderItem.CUSTOMER;
                                DeliveryRequest.EXTERNORDERKEY = ProductItem.DELIVERYCODE;
                                var GetData = HttpClientHelper.POST_JSON(ServiceURL + "/API/Delivery/Status", JsonConvert.SerializeObject(DeliveryRequest));
                                DeliveryResponseModel DeliveryResponse = JsonConvert.DeserializeObject<DeliveryResponseModel>(HttpClientHelper.ConvertString(GetData));

                                if (DeliveryResponse.result == "1")
                                {
                                    OrderQueryProduct.DELIVERYSTATUS = DeliveryResponse.data.FirstOrDefault().SSTS;
                                }
                                else
                                {
                                    OrderQueryProduct.DELIVERYSTATUS = DeliveryResponse.message;
                                }
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(OrderQueryProduct.PRICETYPE)) 
                        {
                            OrderQueryProduct.Giveaway = GiveawayQueryItem;
                            OrderQueryProductList.Add(OrderQueryProduct);
                            OrderQueryProduct = new OrderQueryProductModel();
                        }

                        #endregion 設定訂單副檔

                        #region 設定訂單主檔

                        OrderQueryResponse.data.Add(new OrderQueryItemModel() {
                            ID = OrderItem.CUSTOMER,
                            DEPARTMENT = OrderItem.DEPARTMENT,
                            WAREHOUSE = WAREHOUSE,
                            TYPE = OrderItem.TYPE,
                            PURCHASEORDERCODE = OrderItem.PURCHASEORDERCODE,
                            CITY = OrderItem.CITY,
                            ADDRESS = OrderItem.ADDRESS,
                            PHONE = OrderItem.PHONE,
                            MOBILE = OrderItem.MOBILE,
                            RECEIVER = OrderItem.RECEIVER,
                            DELIVERYINSTRUCTIONCODE = OrderItem.DELIVERYINSTRUCTIONCODE,
                            NOTE = OrderItem.NOTE,
                            SHIPPINGTIME = OrderItem.SHIPPINGTIME,
                            CODE = OrderItem.CODE,
                            LOCKED = OrderItem.LOCKED,
                            BATCH = OrderItem.BATCH,
                            STATUS = OrderItem.STATUS,
                            //0:已刪除/1:未送出/2:修改中/3:已送出/4:有缺貨/5:已出貨/6:已到貨
                            MESSAGE = (OrderItem.STATUS == "0" ? "已刪除" : 
                                      (OrderItem.STATUS == "1" ? "未送出" :
                                      (OrderItem.STATUS == "2" ? "修改中" :
                                      (OrderItem.STATUS == "3" ? "已送出" :
                                      (OrderItem.STATUS == "4" ? "有缺貨" :
                                      (OrderItem.STATUS == "5" ? "已出貨" :
                                      (OrderItem.STATUS == "6" ? "已到貨" : "狀態不明"))))))),
                            CREATE_TIME = OrderItem.CREATE_TIME,
                            ProductData = OrderQueryProductList
                        });

                        #endregion 設定訂單主檔
                    }

                    OrderQueryResponse.result = "1";
                    OrderQueryResponse.message = "Success";
                }
                else
                {
                    OrderQueryResponse.result = "0";
                    OrderQueryResponse.message = "Error:/Order/Query:ID檢核不符";
                }
            }
            catch (Exception ex)
            {
                logger.Error("/Order/Query - " + ex.Message);
                OrderQueryResponse.result = "0";
                OrderQueryResponse.message = "Exception:/Order/Query:" + ex.Message;
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return OrderQueryResponse;
        }

        #endregion 查詢訂單

        #endregion 前臺

        #region 後臺

        #region 查詢訂單

        [HttpPost]
        [CorsHandle]
        public async Task<List<OrderQueryItemModel>> Get()
        {
            List<OrderQueryItemModel> OrderQueryResponse = new List<OrderQueryItemModel>();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var ConfigPath = await TecoApp.Config.Where(p => p.Code == "RootFilePath" && p.Enable == "Y").AsNoTracking().ToListAsync();
                var RootFilePath = ConfigPath.FirstOrDefault().Value + "O\\";

                if (!Request.Content.IsMimeMultipartContent())
                {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                }

                if (!Directory.Exists(RootFilePath))
                {
                    Directory.CreateDirectory(RootFilePath);
                }

                var StreamProvider = new CustomMultipartFormDataStreamProvider(RootFilePath);

                await Request.Content.ReadAsMultipartAsync(StreamProvider);

                var OrderGetRequest = new OrderGetRequestModel();

                foreach (var Key in StreamProvider.FormData.AllKeys)
                {
                    foreach (var Value in StreamProvider.FormData.GetValues(Key))
                    {
                        OrderGetRequest = JsonConvert.DeserializeObject<OrderGetRequestModel>(Value);
                    }
                }

                var ConfigURL = await TecoApp.Config.Where(p => p.Code == "ServiceURL" && p.Enable == "Y").AsNoTracking().ToListAsync();
                var ServiceURL = ConfigURL.FirstOrDefault().Value;

                //取得子單號對應的主單號
                string DELIVERYCODE_MAIN = "";
                if (!string.IsNullOrWhiteSpace(OrderGetRequest.DELIVERYCODE))
                {
                    var temp_OrderItem = await TecoApp.OrderItem.Where(p => p.DELIVERYCODE == OrderGetRequest.DELIVERYCODE).AsNoTracking().ToListAsync();
                    DELIVERYCODE_MAIN = temp_OrderItem.FirstOrDefault().ORDERCODE;
                }

                var OrderList = TecoApp.Order.OrderByDescending(p => p.CREATE_TIME).AsEnumerable();

                if (!string.IsNullOrWhiteSpace(OrderGetRequest.CODE)) 
                {
                    if (!string.IsNullOrWhiteSpace(DELIVERYCODE_MAIN))
                    {
                        OrderList = OrderList.Where(p => p.CODE == OrderGetRequest.CODE || p.CODE == DELIVERYCODE_MAIN);
                    }
                    else
                    {
                        OrderList = OrderList.Where(p => p.CODE == OrderGetRequest.CODE);
                    }
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(DELIVERYCODE_MAIN)) 
                    {
                        OrderList = OrderList.Where(p => p.CODE == DELIVERYCODE_MAIN);
                    }
                }

                if (!string.IsNullOrWhiteSpace(OrderGetRequest.CUSTOMER))
                {
                    OrderList = OrderList.Where(p => p.CUSTOMER == OrderGetRequest.CUSTOMER);
                }

                if (OrderGetRequest.START_TIME.HasValue)
                {
                    DateTime Start = Convert.ToDateTime(OrderGetRequest.START_TIME.Value.ToString("yyyy-MM-dd") + "T00:00:00");
                    OrderList = OrderList.Where(p => p.CREATE_TIME >= Start);
                }

                if (OrderGetRequest.END_TIME.HasValue)
                {
                    DateTime End = Convert.ToDateTime(OrderGetRequest.END_TIME.Value.ToString("yyyy-MM-dd") + "T23:59:59");
                    OrderList = OrderList.Where(p => p.CREATE_TIME <= End);
                }

                OrderList = OrderList.ToList();

                foreach (var OrderItem in OrderList)
                {
                    var ProductList = await TecoApp.OrderItem.Where(p => p.ORDERCODE == OrderItem.CODE).AsNoTracking().ToListAsync();
                    var WAREHOUSE = "";

                    List<OrderQueryProductModel> OrderQueryProductList = new List<OrderQueryProductModel>();
                    OrderQueryProductModel OrderQueryProduct = new OrderQueryProductModel();
                    List<GiveawayQueryItemModel> GiveawayQueryItem = new List<GiveawayQueryItemModel>();

                    #region 設定訂單副檔

                    var CheckFirst = true;
                    foreach (var ProductItem in ProductList)
                    {
                        if (WAREHOUSE == "")
                        {
                            WAREHOUSE = ProductItem.WAREHOUSE;
                        }

                        if (ProductItem.ATTRIBUTIONTYPE == "1")
                        {
                            if (CheckFirst)
                            {
                                CheckFirst = false;
                            }
                            else
                            {
                                OrderQueryProduct.Giveaway = GiveawayQueryItem;
                                OrderQueryProductList.Add(OrderQueryProduct);
                                OrderQueryProduct = new OrderQueryProductModel();
                                GiveawayQueryItem = new List<GiveawayQueryItemModel>();
                            }

                            OrderQueryProduct.PRICETYPE = ProductItem.PRICETYPE;
                            OrderQueryProduct.PRODUCTCODE = ProductItem.PRODUCTCODE;
                            OrderQueryProduct.PRODUCTQUANTITY = ProductItem.QUANTITY;
                            OrderQueryProduct.SHIPPINGTIME = ProductItem.SHIPPINGTIME;
                            OrderQueryProduct.SALESCODE = ProductItem.SALESCODE;
                            OrderQueryProduct.DELIVERYCODE = ProductItem.DELIVERYCODE;
                            OrderQueryProduct.POSTINGCODE = ProductItem.POSTINGCODE;
                            OrderQueryProduct.INVOICECODE = ProductItem.INVOICECODE;
                            OrderQueryProduct.ERRORTYPE = ProductItem.ERRORTYPE;
                            OrderQueryProduct.ERRORMESSAGE = ProductItem.ERRORMESSAGE;
                            OrderQueryProduct.ERRORDATE = ProductItem.ERRORDATE;
                            OrderQueryProduct.ERRORTIME = ProductItem.ERRORTIME;
                        }
                        else if (ProductItem.ATTRIBUTIONTYPE == "2")
                        {
                            OrderQueryProduct.RELATEDPRODUCTCODE = ProductItem.PRODUCTCODE;
                            OrderQueryProduct.RELATEDPRODUCTQUANTITY = ProductItem.QUANTITY;
                            OrderQueryProduct.SHIPPINGTIME = ProductItem.SHIPPINGTIME;
                            OrderQueryProduct.SALESCODE = ProductItem.SALESCODE;
                            OrderQueryProduct.DELIVERYCODE = ProductItem.DELIVERYCODE;
                            OrderQueryProduct.POSTINGCODE = ProductItem.POSTINGCODE;
                            OrderQueryProduct.INVOICECODE = ProductItem.INVOICECODE;
                            OrderQueryProduct.ERRORTYPE = ProductItem.ERRORTYPE;
                            OrderQueryProduct.ERRORMESSAGE = ProductItem.ERRORMESSAGE;
                            OrderQueryProduct.ERRORDATE = ProductItem.ERRORDATE;
                            OrderQueryProduct.ERRORTIME = ProductItem.ERRORTIME;
                        }
                        else if (ProductItem.ATTRIBUTIONTYPE == "3")
                        {
                            GiveawayQueryItem.Add(new GiveawayQueryItemModel()
                            {
                                PRODUCTCODE = ProductItem.PRODUCTCODE,
                                PRODUCTQUANTITY = ProductItem.QUANTITY,
                                SHIPPINGTIME = ProductItem.SHIPPINGTIME,
                                SALESCODE = ProductItem.SALESCODE,
                                DELIVERYCODE = ProductItem.DELIVERYCODE,
                                POSTINGCODE = ProductItem.POSTINGCODE,
                                INVOICECODE = ProductItem.INVOICECODE,
                                ERRORTYPE = ProductItem.ERRORTYPE,
                                ERRORMESSAGE = ProductItem.ERRORMESSAGE,
                                ERRORDATE = ProductItem.ERRORDATE,
                                ERRORTIME = ProductItem.ERRORTIME
                            });
                        }

                        if (string.IsNullOrWhiteSpace(ProductItem.DELIVERYCODE))
                        {
                            DeliveryRequestModel DeliveryRequest = new DeliveryRequestModel();
                            DeliveryRequest.ID = OrderItem.CUSTOMER;
                            DeliveryRequest.EXTERNORDERKEY = ProductItem.DELIVERYCODE;
                            var GetData = HttpClientHelper.POST_JSON(ServiceURL + "/API/Delivery/Status", JsonConvert.SerializeObject(DeliveryRequest));
                            DeliveryResponseModel DeliveryResponse = JsonConvert.DeserializeObject<DeliveryResponseModel>(HttpClientHelper.ConvertString(GetData));

                            if (DeliveryResponse.result == "1")
                            {
                                OrderQueryProduct.DELIVERYSTATUS = DeliveryResponse.data.FirstOrDefault().SSTS;
                            }
                            else
                            {
                                OrderQueryProduct.DELIVERYSTATUS = DeliveryResponse.message;
                            }
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(OrderQueryProduct.PRICETYPE))
                    {
                        OrderQueryProduct.Giveaway = GiveawayQueryItem;
                        OrderQueryProductList.Add(OrderQueryProduct);
                        OrderQueryProduct = new OrderQueryProductModel();
                    }

                    #endregion 設定訂單副檔

                    #region 設定訂單主檔

                    OrderQueryResponse.Add(new OrderQueryItemModel()
                    {
                        ID = OrderItem.CUSTOMER,
                        DEPARTMENT = OrderItem.DEPARTMENT,
                        WAREHOUSE = WAREHOUSE,
                        TYPE = OrderItem.TYPE,
                        PURCHASEORDERCODE = OrderItem.PURCHASEORDERCODE,
                        CITY = OrderItem.CITY,
                        ADDRESS = OrderItem.ADDRESS,
                        PHONE = OrderItem.PHONE,
                        MOBILE = OrderItem.MOBILE,
                        RECEIVER = OrderItem.RECEIVER,
                        DELIVERYINSTRUCTIONCODE = OrderItem.DELIVERYINSTRUCTIONCODE,
                        NOTE = OrderItem.NOTE,
                        SHIPPINGTIME = OrderItem.SHIPPINGTIME,
                        CODE = OrderItem.CODE,
                        LOCKED = OrderItem.LOCKED,
                        BATCH = OrderItem.BATCH,
                        STATUS = OrderItem.STATUS,
                        //0:已刪除/1:未送出/2:修改中/3:已送出/4:有缺貨/5:已出貨/6:已到貨
                        MESSAGE = (OrderItem.STATUS == "0" ? "已刪除" :
                                    (OrderItem.STATUS == "1" ? "未送出" :
                                    (OrderItem.STATUS == "2" ? "修改中" :
                                    (OrderItem.STATUS == "3" ? "已送出" :
                                    (OrderItem.STATUS == "4" ? "有缺貨" :
                                    (OrderItem.STATUS == "5" ? "已出貨" :
                                    (OrderItem.STATUS == "6" ? "已到貨" : "狀態不明"))))))),
                        CREATE_TIME = OrderItem.CREATE_TIME,
                        ProductData = OrderQueryProductList
                    });

                    #endregion 設定訂單主檔
                }
            }
            catch (Exception ex)
            {
                logger.Error("/Order/Get - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return OrderQueryResponse;
        }

        #endregion 查詢訂單

        #region 訂單批次處理

        [HttpGet]
        public void Batch()
        {
            TecoAppEntities TecoApp = new TecoAppEntities();
            OrderRFCRequestModel OrderRFCRequest = new OrderRFCRequestModel();

            try
            {
                logger.Info("/Order/Batch - Start");

                List<OrderRFCItemModel> OrderRFCItem = new List<OrderRFCItemModel>();

                #region 判斷未完成的訂單商品，進行下單前置參數設定

                var OrderItemCheckList = TecoApp.OrderItem.AsEnumerable().Where(p => p.DELIVERYCODE == "" && p.ERRORMESSAGE == "").ToList();
                var CheckDone = "";

                foreach (var CheckOrderItem in OrderItemCheckList)
                {
                    if (!CheckDone.Contains(CheckOrderItem.ORDERCODE))
                    {
                        CheckDone += CheckOrderItem.ORDERCODE + ",";

                        var OrderListCheckList = TecoApp.Order.AsEnumerable().Where(p => p.CODE == CheckOrderItem.ORDERCODE && p.LOCKED == "N" && p.BATCH == "Y" && (p.STATUS == "3" || p.STATUS == "4")).ToList();
                        foreach (var CheckOrderList in OrderListCheckList)
                        {
                            //批次註記
                            CheckOrderList.BATCH = "N";
                            CheckOrderList.STATUS = "1";
                            TecoApp.SaveChanges();
                        }
                    }
                }

                #endregion 判斷未完成的訂單商品，進行下單前置參數設定

                var ConfigList = TecoApp.Config.AsEnumerable().Where(p => p.Code == "ServiceURL" && p.Enable == "Y").ToList();
                var ServiceURL = ConfigList.FirstOrDefault().Value;
                var OrderList = TecoApp.Order.AsEnumerable().Where(p => p.LOCKED == "N" && p.BATCH == "N" && p.STATUS == "1").ToList();

                foreach (var OrderItem in OrderList)
                {
                    //批次註記
                    OrderItem.BATCH = "Y";
                    TecoApp.SaveChanges();
                }

                foreach (var OrderItem in OrderList)
                {
                    var OrderTime = ((OrderItem.UPDATE_TIME != null && OrderItem.UPDATE_TIME.HasValue) ? (DateTime)OrderItem.UPDATE_TIME : OrderItem.CREATE_TIME);
                    var NowTime = DateTime.Now;
                    var TimeSpan = new TimeSpan(NowTime.Ticks - OrderTime.Ticks);

                    //判斷時間(0分鐘)
                    if (TimeSpan.TotalMinutes >= 0)
                    {
                        logger.Info("/Order/Batch - Have Order (Prepare to Send)");

                        //取得所有倉庫編號
                        var WareHouseList = TecoApp.WareHouse.AsEnumerable().ToList();

                        //取得部門資料
                        var CDAList = TecoApp.CustomerDepartmentAttribution.AsEnumerable().Where(p => p.CUSTOMERID == OrderItem.CUSTOMER).ToList();
                        var CustomerName = CDAList.FirstOrDefault().CUSTOMERNAME;
                        var DepartmentR = "";
                        var DepartmentK = "";
                        foreach (var CDAItem in CDAList)
                        {
                            if (CDAItem.GROUPCODE.Contains("R"))
                            {
                                DepartmentR = CDAItem.DEPARTMENTID;
                            }
                            else if (CDAItem.GROUPCODE.Contains("K"))
                            {
                                DepartmentK = CDAItem.DEPARTMENTID;
                            }
                        }

                        var PerfectOrder = true;
                        var Post3 = true;
                        var OrderNo = 10;
                        var ProductList = TecoApp.OrderItem.AsEnumerable().Where(p => p.ORDERCODE == OrderItem.CODE).ToList();

                        for (int i = 0; i < ProductList.Count(); i++)
                        {
                            //沒處理過才送
                            if (string.IsNullOrWhiteSpace(ProductList[i].DELIVERYCODE) && string.IsNullOrWhiteSpace(ProductList[i].ERRORMESSAGE))
                            {
                                //設定部門編號
                                var GiveawayList = TecoApp.Giveaway.AsEnumerable().Where(p => p.PRODUCTCODE == ProductList[i].PRODUCTCODE).ToList();
                                var Department = (ProductList[i].ATTRIBUTIONTYPE == "3" ? (GiveawayList.FirstOrDefault().TYPE.Contains("K") ? DepartmentK : DepartmentR) : DepartmentR);

                                //設定工廠編號
                                var Factory = (ProductList[i].ATTRIBUTIONTYPE == "3" ? (GiveawayList.FirstOrDefault().TYPE.Contains("K") ? "12K0" : "15R0") : "15R0");

                                //設定組織編號
                                var GroupType = (ProductList[i].ATTRIBUTIONTYPE == "3" ? (GiveawayList.FirstOrDefault().TYPE.Contains("K") ? "K000" : "R000") : "R000");

                                //設定訂單副檔
                                OrderRFCItem.Add(new OrderRFCItemModel()
                                {
                                    POSNR = ProductList[i].CODE,
                                    MATNR = ProductList[i].PRODUCTCODE,
                                    WERKS = Factory,
                                    LGORT = ProductList[i].WAREHOUSE,
                                    KWMENG = ProductList[i].QUANTITY,
                                    CO_NO = ProductList[i].ORDERCODE,
                                    CO_SEQ = OrderNo.ToString()
                                });

                                if (i == (ProductList.Count() - 1) ||
                                    (ProductList[i].ATTRIBUTIONTYPE == "1" && ProductList[i + 1].ATTRIBUTIONTYPE != "2") ||
                                    ProductList[i].ATTRIBUTIONTYPE == "2" ||
                                    ProductList[i].ATTRIBUTIONTYPE == "3")
                                {
                                    //設定訂單主檔
                                    OrderRFCRequest = new OrderRFCRequestModel()
                                    {
                                        ZAUART = OrderItem.TYPE,
                                        ZVKORG = GroupType,
                                        ZVTWEG = ProductList[i].PRICETYPE,
                                        ZSPART = Department,
                                        ZKUNNR = OrderItem.CUSTOMER,
                                        ZKUNWE = OrderItem.CUSTOMER,
                                        ZBSTKD = (string.IsNullOrWhiteSpace(OrderItem.PURCHASEORDERCODE) ? "" : OrderItem.PURCHASEORDERCODE),
                                        ZCITY1 = OrderItem.CITY,
                                        ZSTREET = OrderItem.ADDRESS,
                                        ZTELF1 = OrderItem.PHONE,
                                        ZTEL_NUMBER = OrderItem.MOBILE,
                                        ZANRED = OrderItem.RECEIVER,
                                        ZMARK = (string.IsNullOrWhiteSpace(OrderItem.DELIVERYINSTRUCTIONCODE) ? OrderItem.NOTE : OrderItem.DELIVERYINSTRUCTIONCODE + "." + OrderItem.NOTE),
                                        data = OrderRFCItem
                                    };

                                    if (ProductList[i].ATTRIBUTIONTYPE == "1" ||
                                        ProductList[i].ATTRIBUTIONTYPE == "2" ||
                                        ProductList[i].ATTRIBUTIONTYPE == "3" && Post3)
                                    {
                                        #region (下單前)查詢庫存

                                        MARDRequestModel MARDRequest = new MARDRequestModel();
                                        foreach (var MARDItem in OrderRFCRequest.data)
                                        {
                                            MARDRequest.data.Add(new MARDQueryModel() {
                                                WERKS = MARDItem.WERKS,
                                                LGORT = MARDItem.LGORT,
                                                MATNR = MARDItem.MATNR
                                            });
                                        }

                                        logger.Info("/Order/Batch - Order Data : " + JsonConvert.SerializeObject(OrderRFCRequest));

                                        var GetMARD = HttpClientHelper.POST_JSON(ServiceURL + "/API/MARD/Query", JsonConvert.SerializeObject(MARDRequest));
                                        MARDResponseModel MARDResponse = JsonConvert.DeserializeObject<MARDResponseModel>(HttpClientHelper.ConvertString(GetMARD));

                                        #endregion (下單前)查詢庫存

                                        #region 判斷商品庫存即時狀態/移倉

                                        //0:訂單批次處理異常/系統異常/無法送單
                                        //1:庫存足額/可下單 2:所有倉庫庫存不足/移倉失敗
                                        //3:庫存查詢不到/查詢失敗 4:東元RFC系統異常/系統異常/無法送單
                                        string[] MARDErrorMessage = new string[] { 
                                            "訂單批次處理異常/系統異常/無法送單", 
                                            "庫存足額/可下單", "所有倉庫庫存不足/移倉失敗", 
                                            "庫存查詢不到/查詢失敗", "東元RFC系統異常/系統異常/無法送單" 
                                        };
                                        int OrderRFCRequest1OK = 0;
                                        int OrderRFCRequest2OK = 0;
                                        string OrderRFCRequest1ShippingTime = "0";
                                        string OrderRFCRequest2ShippingTime = "0";
                                        int CheckMARD = 0;

                                        if (MARDResponse.result == "1")
                                        {
                                            int ItemCount = 0;

                                            foreach (var OrderRFCReq in OrderRFCRequest.data)
                                            {
                                                ItemCount++;

                                                foreach (var MARDResp in MARDResponse.data)
                                                {
                                                    if (OrderRFCReq.MATNR == MARDResp.MATNR)
                                                    {
                                                        //訂購數量 <= 庫存數量
                                                        if (Convert.ToDouble(OrderRFCReq.KWMENG) <= Convert.ToDouble(MARDResp.LABST))
                                                        {
                                                            #region 庫存足額/可下單

                                                            if (ProductList[i].ATTRIBUTIONTYPE == "2") //複數商品
                                                            {
                                                                if (ItemCount == 1) 
                                                                {
                                                                    OrderRFCRequest1OK = 1;
                                                                    OrderRFCRequest1ShippingTime = "0";
                                                                }
                                                                else 
                                                                {
                                                                    OrderRFCRequest2OK = 1;
                                                                    OrderRFCRequest2ShippingTime = "0";
                                                                }
                                                            }
                                                            else //單數商品
                                                            {
                                                                OrderRFCRequest1OK = 1;
                                                                OrderRFCRequest1ShippingTime = "0";
                                                            }

                                                            #endregion 庫存足額/可下單
                                                        }
                                                        else
                                                        {
                                                            #region 庫存移倉

                                                            //取得倉庫關聯設定
                                                            var WareHouseRelatedList = TecoApp.WareHouseRelated.Where(p => p.ToWhere == OrderRFCReq.LGORT).OrderBy(p => p.ShippingRate).ThenBy(p => p.DelayShippingTime).AsEnumerable().ToList();

                                                            #region (移倉前)查詢庫存(所有倉庫)

                                                            System.Threading.Thread.Sleep(1000);

                                                            MARDRequestModel MARDTransferRequest = new MARDRequestModel();
                                                            foreach (var WareHouse in WareHouseList)
                                                            {
                                                                if (WareHouse.CODE != OrderRFCReq.LGORT)
                                                                {
                                                                    MARDTransferRequest.data.Add(new MARDQueryModel()
                                                                    {
                                                                        WERKS = OrderRFCReq.WERKS,
                                                                        LGORT = WareHouse.CODE,
                                                                        MATNR = OrderRFCReq.MATNR
                                                                    });
                                                                }
                                                            }

                                                            logger.Info("/Order/Batch - MARDTransfer Data : " + JsonConvert.SerializeObject(MARDTransferRequest));

                                                            var GetMARDTransfer = HttpClientHelper.POST_JSON(ServiceURL + "/API/MARD/Query", JsonConvert.SerializeObject(MARDTransferRequest));
                                                            MARDResponseModel MARDTransferResponse = JsonConvert.DeserializeObject<MARDResponseModel>(HttpClientHelper.ConvertString(GetMARDTransfer));

                                                            #endregion (移倉前)查詢庫存(所有倉庫)

                                                            if (MARDTransferResponse.result == "1" && MARDTransferResponse.data.Count() > 0)
                                                            {
                                                                #region 判斷移倉倉庫與數量

                                                                //需求數量 = 訂購數量 - 現有庫存
                                                                double DemandCount = Convert.ToDouble(OrderRFCReq.KWMENG) - Convert.ToDouble(MARDResp.LABST);
                                                                //移倉數量
                                                                double GetItemCount = 0;
                                                                //移倉內容:數量,倉庫(複數倉庫以;分隔)
                                                                string WareHouseTransfer = "";

                                                                //優先判斷K900總倉
                                                                bool K900Ready = false;
                                                                foreach (var WareHouseRelated in WareHouseRelatedList)
                                                                {
                                                                    if (WareHouseRelated.FromWhere.Contains("K900"))
                                                                    {
                                                                        foreach (var MARDTransferResponseData in MARDTransferResponse.data)
                                                                        {
                                                                            if (WareHouseRelated.FromWhere == MARDTransferResponseData.LGORT &&
                                                                                WareHouseRelated.FromWhere.Contains("K900"))
                                                                            {
                                                                                double CheckCount = Convert.ToDouble(MARDTransferResponseData.LABST);

                                                                                if (CheckCount > 0)
                                                                                {
                                                                                    //設定運送日
                                                                                    if (ProductList[i].ATTRIBUTIONTYPE == "2") //複數商品
                                                                                    {
                                                                                        if (ItemCount == 1)
                                                                                        {
                                                                                            OrderRFCRequest1ShippingTime = WareHouseRelated.DelayShippingTime;
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            OrderRFCRequest2ShippingTime = WareHouseRelated.DelayShippingTime;
                                                                                        }
                                                                                    }
                                                                                    else //單數商品
                                                                                    {
                                                                                        OrderRFCRequest1ShippingTime = WareHouseRelated.DelayShippingTime;
                                                                                    }

                                                                                    //設定移倉資料
                                                                                    if (CheckCount >= (DemandCount - GetItemCount))
                                                                                    {
                                                                                        WareHouseTransfer += ";" + (DemandCount - GetItemCount).ToString() + "," + MARDTransferResponseData.LGORT;
                                                                                        GetItemCount += DemandCount - GetItemCount;
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        WareHouseTransfer += ";" + CheckCount.ToString() + "," + MARDTransferResponseData.LGORT;
                                                                                        GetItemCount += CheckCount;
                                                                                    }
                                                                                }

                                                                                K900Ready = true;
                                                                                break;
                                                                            }
                                                                        }
                                                                    }

                                                                    if (K900Ready)
                                                                    {
                                                                        break;
                                                                    }
                                                                }

                                                                //判斷去除K900總倉的其他倉庫
                                                                if (DemandCount != GetItemCount)
                                                                {
                                                                    foreach (var WareHouseRelated in WareHouseRelatedList)
                                                                    {
                                                                        if (!WareHouseRelated.FromWhere.Contains("K900"))
                                                                        {
                                                                            foreach (var MARDTransferResponseData in MARDTransferResponse.data)
                                                                            {
                                                                                if (WareHouseRelated.FromWhere == MARDTransferResponseData.LGORT &&
                                                                                    !WareHouseRelated.FromWhere.Contains("K900"))
                                                                                {
                                                                                    double CheckCount = Convert.ToDouble(MARDTransferResponseData.LABST);

                                                                                    if (CheckCount > 0)
                                                                                    {
                                                                                        //設定運送日
                                                                                        if (ProductList[i].ATTRIBUTIONTYPE == "2") //複數商品
                                                                                        {
                                                                                            if (ItemCount == 1)
                                                                                            {
                                                                                                OrderRFCRequest1ShippingTime = WareHouseRelated.DelayShippingTime;
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                OrderRFCRequest2ShippingTime = WareHouseRelated.DelayShippingTime;
                                                                                            }
                                                                                        }
                                                                                        else //單數商品
                                                                                        {
                                                                                            OrderRFCRequest1ShippingTime = WareHouseRelated.DelayShippingTime;
                                                                                        }

                                                                                        //設定移倉資料
                                                                                        if (CheckCount >= (DemandCount - GetItemCount))
                                                                                        {
                                                                                            WareHouseTransfer += ";" + (DemandCount - GetItemCount).ToString() + "," + MARDTransferResponseData.LGORT;
                                                                                            GetItemCount += DemandCount - GetItemCount;
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            WareHouseTransfer += ";" + CheckCount.ToString() + "," + MARDTransferResponseData.LGORT;
                                                                                            GetItemCount += CheckCount;
                                                                                        }
                                                                                    }

                                                                                    break;
                                                                                }
                                                                            }
                                                                        }

                                                                        if (DemandCount == GetItemCount)
                                                                        {
                                                                            break;
                                                                        }
                                                                    }
                                                                }

                                                                //移倉後數量足夠才移倉
                                                                if (DemandCount == GetItemCount)
                                                                {
                                                                    #region 移倉內容

                                                                    TransferRequestModel TransferRequest = new TransferRequestModel();
                                                                    var TransferList = WareHouseTransfer.Split(';').ToList();
                                                                    foreach (var TransferData in TransferList)
                                                                    {
                                                                        if (!string.IsNullOrWhiteSpace(TransferData))
                                                                        {
                                                                            var TransferMENGE = TransferData.Split(',')[0];
                                                                            var TransferLGORT = TransferData.Split(',')[1];

                                                                            TransferRequest.data.Add(new TransferModel()
                                                                            {
                                                                                BWART = "311",
                                                                                WERKS = OrderRFCReq.WERKS,
                                                                                LGORT = TransferLGORT,
                                                                                UMLGO = OrderRFCReq.LGORT,
                                                                                MATNR = OrderRFCReq.MATNR,
                                                                                MENGE = TransferMENGE,
                                                                                ITEM_TXT = "客戶缺貨，從" + TransferLGORT + "，調" + TransferMENGE + "單位，到" + OrderRFCReq.LGORT
                                                                            });
                                                                        }
                                                                    }

                                                                    logger.Info("/Order/Batch - Transferring Data : " + JsonConvert.SerializeObject(TransferRequest));

                                                                    var GetTransfer = HttpClientHelper.POST_JSON(ServiceURL + "/API/MARD/Transfer", JsonConvert.SerializeObject(TransferRequest));
                                                                    TransferResponseModel TransferResponse = JsonConvert.DeserializeObject<TransferResponseModel>(HttpClientHelper.ConvertString(GetTransfer));

                                                                    #endregion 移倉內容

                                                                    if (TransferResponse.result == "1")
                                                                    {
                                                                        #region (移倉後)查詢庫存

                                                                        System.Threading.Thread.Sleep(1000);

                                                                        MARDRequestModel MARDTransferResultRequest = new MARDRequestModel();
                                                                        MARDTransferResultRequest.data.Add(new MARDQueryModel()
                                                                        {
                                                                            WERKS = OrderRFCReq.WERKS,
                                                                            LGORT = OrderRFCReq.LGORT,
                                                                            MATNR = OrderRFCReq.MATNR
                                                                        });

                                                                        logger.Info("/Order/Batch - MARDTransferResult Data : " + JsonConvert.SerializeObject(MARDTransferResultRequest));

                                                                        var GetMARDTransferResult = HttpClientHelper.POST_JSON(ServiceURL + "/API/MARD/Query", JsonConvert.SerializeObject(MARDTransferResultRequest));
                                                                        MARDResponseModel MARDTransferResultResponse = JsonConvert.DeserializeObject<MARDResponseModel>(HttpClientHelper.ConvertString(GetMARDTransferResult));

                                                                        #endregion (移倉後)查詢庫存

                                                                        int MARDTransferCheckTimes = 5;

                                                                        //重複查詢庫存
                                                                        while (MARDTransferCheckTimes > 0)
                                                                        {
                                                                            MARDTransferCheckTimes--;

                                                                            if (MARDTransferResultResponse.result == "1" && MARDTransferResultResponse.data.Count() > 0)
                                                                            {
                                                                                if (Convert.ToDouble(OrderRFCReq.KWMENG) <= Convert.ToDouble(MARDTransferResultResponse.data[0].LABST))
                                                                                {
                                                                                    #region 庫存足額/可下單

                                                                                    if (ProductList[i].ATTRIBUTIONTYPE == "2") //複數商品
                                                                                    {
                                                                                        if (ItemCount == 1)
                                                                                        {
                                                                                            OrderRFCRequest1OK = 1;
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            OrderRFCRequest2OK = 1;
                                                                                        }
                                                                                    }
                                                                                    else //單數商品
                                                                                    {
                                                                                        OrderRFCRequest1OK = 1;
                                                                                    }

                                                                                    #endregion 庫存足額/可下單

                                                                                    break;
                                                                                }
                                                                                else
                                                                                {
                                                                                    #region 所有倉庫庫存不足/移倉失敗

                                                                                    if (ProductList[i].ATTRIBUTIONTYPE == "2") //複數商品
                                                                                    {
                                                                                        if (ItemCount == 1)
                                                                                        {
                                                                                            OrderRFCRequest1OK = 2;
                                                                                            OrderRFCRequest1ShippingTime = "X";
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            OrderRFCRequest2OK = 2;
                                                                                            OrderRFCRequest2ShippingTime = "X";
                                                                                        }
                                                                                    }
                                                                                    else //單數商品
                                                                                    {
                                                                                        OrderRFCRequest1OK = 2;
                                                                                        OrderRFCRequest1ShippingTime = "X";
                                                                                    }

                                                                                    #endregion 所有倉庫庫存不足/移倉失敗

                                                                                    //最後一次不做查詢
                                                                                    if (MARDTransferCheckTimes > 1)
                                                                                    {
                                                                                        System.Threading.Thread.Sleep(1000);

                                                                                        GetMARDTransferResult = HttpClientHelper.POST_JSON(ServiceURL + "/API/MARD/Query", JsonConvert.SerializeObject(MARDTransferResultRequest));
                                                                                        MARDTransferResultResponse = JsonConvert.DeserializeObject<MARDResponseModel>(HttpClientHelper.ConvertString(GetMARDTransferResult));
                                                                                    }
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                #region 所有倉庫庫存不足/移倉失敗

                                                                                if (ProductList[i].ATTRIBUTIONTYPE == "2") //複數商品
                                                                                {
                                                                                    if (ItemCount == 1)
                                                                                    {
                                                                                        OrderRFCRequest1OK = 2;
                                                                                        OrderRFCRequest1ShippingTime = "X";
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        OrderRFCRequest2OK = 2;
                                                                                        OrderRFCRequest2ShippingTime = "X";
                                                                                    }
                                                                                }
                                                                                else //單數商品
                                                                                {
                                                                                    OrderRFCRequest1OK = 2;
                                                                                    OrderRFCRequest1ShippingTime = "X";
                                                                                }

                                                                                #endregion 所有倉庫庫存不足/移倉失敗
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        #region 所有倉庫庫存不足/移倉失敗

                                                                        if (ProductList[i].ATTRIBUTIONTYPE == "2") //複數商品
                                                                        {
                                                                            if (ItemCount == 1)
                                                                            {
                                                                                OrderRFCRequest1OK = 2;
                                                                                OrderRFCRequest1ShippingTime = "X";
                                                                            }
                                                                            else
                                                                            {
                                                                                OrderRFCRequest2OK = 2;
                                                                                OrderRFCRequest2ShippingTime = "X";
                                                                            }
                                                                        }
                                                                        else //單數商品
                                                                        {
                                                                            OrderRFCRequest1OK = 2;
                                                                            OrderRFCRequest1ShippingTime = "X";
                                                                        }

                                                                        #endregion 所有倉庫庫存不足/移倉失敗
                                                                    }

                                                                    //紀錄移倉結果
                                                                    foreach (var TransferResultData in TransferResponse.data)
                                                                    {
                                                                        Transfer TransferLog = new Transfer();
                                                                        TransferLog.WERKS = TransferResultData.WERKS;
                                                                        TransferLog.LGORT = TransferResultData.LGORT;
                                                                        TransferLog.UMLGO = TransferResultData.UMLGO;
                                                                        TransferLog.MATNR = TransferResultData.MATNR;
                                                                        TransferLog.MENGE = TransferResultData.MENGE;
                                                                        TransferLog.MBLNR = (string.IsNullOrWhiteSpace(TransferResultData.MBLNR) ? "" : TransferResultData.MBLNR);
                                                                        TransferLog.TYPE = TransferResultData.TYPE;
                                                                        TransferLog.MESSAGE = (string.IsNullOrWhiteSpace(TransferResultData.MESSAGE) ? "" : TransferResultData.MESSAGE);
                                                                        TransferLog.DATUM = TransferResultData.DATUM;
                                                                        TransferLog.UZEIT = TransferResultData.UZEIT;
                                                                        TransferLog.CREATE_TIME = System.DateTime.Now;
                                                                        TransferLog.CREATE_USER = "System";
                                                                        TecoApp.Transfer.Add(TransferLog);
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    #region 所有倉庫庫存不足/移倉失敗

                                                                    if (ProductList[i].ATTRIBUTIONTYPE == "2") //複數商品
                                                                    {
                                                                        if (ItemCount == 1)
                                                                        {
                                                                            OrderRFCRequest1OK = 2;
                                                                            OrderRFCRequest1ShippingTime = "X";
                                                                        }
                                                                        else
                                                                        {
                                                                            OrderRFCRequest2OK = 2;
                                                                            OrderRFCRequest2ShippingTime = "X";
                                                                        }
                                                                    }
                                                                    else //單數商品
                                                                    {
                                                                        OrderRFCRequest1OK = 2;
                                                                        OrderRFCRequest1ShippingTime = "X";
                                                                    }

                                                                    #endregion 所有倉庫庫存不足/移倉失敗
                                                                }

                                                                #endregion 判斷移倉倉庫與數量
                                                            }
                                                            else
                                                            {
                                                                #region 所有倉庫庫存不足/移倉失敗

                                                                if (ProductList[i].ATTRIBUTIONTYPE == "2") //複數商品
                                                                {
                                                                    if (ItemCount == 1)
                                                                    {
                                                                        OrderRFCRequest1OK = 2;
                                                                        OrderRFCRequest1ShippingTime = "X";
                                                                    }
                                                                    else
                                                                    {
                                                                        OrderRFCRequest2OK = 2;
                                                                        OrderRFCRequest2ShippingTime = "X";
                                                                    }
                                                                }
                                                                else //單數商品
                                                                {
                                                                    OrderRFCRequest1OK = 2;
                                                                    OrderRFCRequest1ShippingTime = "X";
                                                                }

                                                                #endregion 所有倉庫庫存不足/移倉失敗
                                                            }

                                                            #endregion 庫存移倉
                                                        }

                                                        break;
                                                    }
                                                }

                                                #region 庫存查詢不到/查詢失敗

                                                if (ProductList[i].ATTRIBUTIONTYPE == "2") //複數商品
                                                {
                                                    if (ItemCount == 1)
                                                    {
                                                        if (OrderRFCRequest1OK == 0)
                                                        {
                                                            OrderRFCRequest1OK = 3;
                                                            OrderRFCRequest1ShippingTime = "X";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (OrderRFCRequest2OK == 0)
                                                        {
                                                            OrderRFCRequest2OK = 3;
                                                            OrderRFCRequest2ShippingTime = "X";
                                                        }
                                                    }
                                                }
                                                else //單數商品
                                                {
                                                    if (OrderRFCRequest1OK == 0)
                                                    {
                                                        OrderRFCRequest1OK = 3;
                                                        OrderRFCRequest1ShippingTime = "X";
                                                    }
                                                }

                                                #endregion 庫存查詢不到/查詢失敗
                                            }

                                            //判斷 1:庫存足額/可下單 2:所有倉庫庫存不足/移倉失敗 3:庫存查詢不到/查詢失敗
                                            if (ProductList[i].ATTRIBUTIONTYPE == "2") //複數商品
                                            {
                                                if (OrderRFCRequest1OK == 1 && OrderRFCRequest2OK == 1)
                                                {
                                                    CheckMARD = 1;
                                                }
                                                else if (OrderRFCRequest1OK == 1)
                                                {
                                                    CheckMARD = OrderRFCRequest2OK;
                                                }
                                                else
                                                {
                                                    CheckMARD = OrderRFCRequest1OK;
                                                }
                                            }
                                            else //單數商品
                                            {
                                                CheckMARD = OrderRFCRequest1OK;
                                            }
                                        }
                                        else //4:東元RFC系統異常/系統異常/無法送單
                                        {
                                            CheckMARD = 4;
                                        }

                                        #endregion 判斷商品庫存即時狀態/移倉

                                        if (CheckMARD == 1) //1:庫存足額/可下單
                                        {
                                            #region 送出訂單

                                            logger.Info("/Order/Batch - Order Data : Check MARD is OK");

                                            //RFC
                                            var GetData = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/ORDER/Add", JsonConvert.SerializeObject(OrderRFCRequest));
                                            OrderResponseModel OrderResponse = JsonConvert.DeserializeObject<OrderResponseModel>(HttpClientHelper.ConvertString(GetData));

                                            //紀錄
                                            if (OrderResponse.result == "1")
                                            {
                                                logger.Info("/Order/Batch - Order Success - Result : " + JsonConvert.SerializeObject(OrderResponse));

                                                //成功
                                                foreach (var ProductItem in ProductList)
                                                {
                                                    if (ProductList[i].ATTRIBUTIONTYPE == "2") //複數商品
                                                    {
                                                        if (ProductItem.ID == ProductList[i - 1].ID)
                                                        {
                                                            ProductItem.SHIPPINGTIME = OrderRFCRequest1ShippingTime;
                                                        }

                                                        if (ProductItem.ID == ProductList[i].ID)
                                                        {
                                                            ProductItem.SHIPPINGTIME = OrderRFCRequest2ShippingTime;
                                                        }

                                                        var No = 0;
                                                        if (ProductItem.ID == ProductList[i - 1].ID
                                                            ||
                                                            ProductItem.ID == ProductList[i].ID)
                                                        {
                                                            No++;
                                                            ProductItem.SALESCODE = OrderResponse.SALESCODE;
                                                            ProductItem.DELIVERYCODE = OrderResponse.DELIVERYCODE;
                                                            ProductItem.POSTINGCODE = OrderResponse.POSTINGCODE;
                                                            ProductItem.INVOICECODE = OrderResponse.INVOICECODE;
                                                            Post3 = true;

                                                            if (No == 2)
                                                            {
                                                                break;
                                                            }
                                                        }
                                                    }
                                                    else //單數商品
                                                    {
                                                        if (ProductItem.ID == ProductList[i].ID)
                                                        {
                                                            ProductItem.SHIPPINGTIME = OrderRFCRequest1ShippingTime;
                                                            ProductItem.SALESCODE = OrderResponse.SALESCODE;
                                                            ProductItem.DELIVERYCODE = OrderResponse.DELIVERYCODE;
                                                            ProductItem.POSTINGCODE = OrderResponse.POSTINGCODE;
                                                            ProductItem.INVOICECODE = OrderResponse.INVOICECODE;
                                                            Post3 = true;
                                                            break;
                                                        }
                                                    }
                                                }

                                                //失敗
                                                foreach (var OrderResponseError in OrderResponse.data)
                                                {
                                                    if (ProductList[i].ATTRIBUTIONTYPE == "2")
                                                    {
                                                        var No = 0;
                                                        foreach (var ProductItem in ProductList)
                                                        {
                                                            if (ProductItem.ID == ProductList[i - 1].ID
                                                                ||
                                                                ProductItem.ID == ProductList[i].ID)
                                                            {
                                                                No++;
                                                                ProductItem.SHIPPINGTIME = "X";
                                                                ProductItem.ERRORTYPE = OrderResponseError.ERRORTYPE;
                                                                ProductItem.ERRORMESSAGE = OrderResponseError.ERRORMESSAGE;
                                                                ProductItem.ERRORDATE = OrderResponseError.ERRORDATE;
                                                                ProductItem.ERRORTIME = OrderResponseError.ERRORTIME;

                                                                if (No == 2)
                                                                {
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                        PerfectOrder = false;
                                                        Post3 = false;
                                                    }
                                                    else
                                                    {
                                                        foreach (var ProductItem in ProductList)
                                                        {
                                                            if (ProductItem.ID == ProductList[i].ID)
                                                            {
                                                                ProductItem.SHIPPINGTIME = "X";
                                                                ProductItem.ERRORTYPE = OrderResponseError.ERRORTYPE;
                                                                ProductItem.ERRORMESSAGE = OrderResponseError.ERRORMESSAGE;
                                                                ProductItem.ERRORDATE = OrderResponseError.ERRORDATE;
                                                                ProductItem.ERRORTIME = OrderResponseError.ERRORTIME;
                                                                break;
                                                            }
                                                        }
                                                        PerfectOrder = false;

                                                        if (ProductList[i].ATTRIBUTIONTYPE == "3")
                                                        {
                                                            Post3 = true;
                                                        }
                                                        else
                                                        {
                                                            Post3 = false;
                                                        }
                                                    }
                                                }

                                                //3:已送出/4:有缺貨
                                                OrderItem.STATUS = (PerfectOrder ? "3" : "4");
                                            }

                                            #endregion 送出訂單
                                        }
                                        else //0:訂單批次處理異常/系統異常/無法送單 2:所有倉庫庫存不足/移倉失敗 3:庫存查詢不到/查詢失敗 4:東元RFC系統異常/系統異常/無法送單
                                        {
                                            #region 庫存有誤(紀錄錯誤訊息)

                                            logger.Info("/Order/Batch - Order Data : Check MARD is Error");

                                            var MARDErrorMessageString = "";

                                            if (CheckMARD == 0 || CheckMARD == 4) //0:訂單批次處理異常/系統異常/無法送單 4:東元RFC系統異常/系統異常/無法送單
                                            {
                                                MARDErrorMessageString = MARDErrorMessage[CheckMARD];
                                            }
                                            else //2:所有倉庫庫存不足/移倉失敗 3:庫存查詢不到/查詢失敗
                                            {
                                                if (ProductList[i].ATTRIBUTIONTYPE == "2") //複數商品
                                                {
                                                    MARDErrorMessageString = "[主商品]" + MARDErrorMessage[OrderRFCRequest1OK] + "/[關聯商品]" + MARDErrorMessage[OrderRFCRequest2OK] + "/無法成單";
                                                }
                                                else //單數商品
                                                {
                                                    MARDErrorMessageString = "[主商品]" + MARDErrorMessage[OrderRFCRequest1OK] + "/無法成單";
                                                }
                                            }

                                            foreach (var ProductItem in ProductList)
                                            {
                                                if (ProductList[i].ATTRIBUTIONTYPE == "2") //複數商品
                                                {
                                                    var No = 0;
                                                    if (ProductItem.ID == ProductList[i - 1].ID
                                                        ||
                                                        ProductItem.ID == ProductList[i].ID)
                                                    {
                                                        No++;
                                                        ProductItem.SHIPPINGTIME = "X";
                                                        ProductItem.ERRORMESSAGE = MARDErrorMessageString;
                                                        Post3 = false;

                                                        if (No == 2)
                                                        {
                                                            break;
                                                        }
                                                    }
                                                }
                                                else //單數商品
                                                {
                                                    if (ProductItem.ID == ProductList[i].ID)
                                                    {
                                                        ProductItem.SHIPPINGTIME = "X";
                                                        ProductItem.ERRORMESSAGE = MARDErrorMessageString;
                                                        Post3 = false;
                                                        break;
                                                    }
                                                }
                                            }

                                            OrderItem.STATUS = "4";

                                            #endregion 庫存有誤(紀錄錯誤訊息)
                                        }

                                        TecoApp.SaveChanges();
                                        OrderRFCItem.Clear();
                                        System.Threading.Thread.Sleep(1000);
                                    }

                                    #region 主商品無成單(紀錄錯誤訊息)

                                    if (ProductList[i].ATTRIBUTIONTYPE == "3" && !Post3)
                                    {
                                        foreach (var ProductItem in ProductList)
                                        {
                                            if (ProductItem.ID == ProductList[i].ID)
                                            {
                                                ProductItem.SHIPPINGTIME = "X";
                                                ProductItem.ERRORMESSAGE = "主商品無成單/贈品不送單";
                                                break;
                                            }
                                        }

                                        TecoApp.SaveChanges();
                                        OrderRFCItem.Clear();
                                    }

                                    #endregion 主商品無成單(紀錄錯誤訊息)
                                }

                                OrderNo += 10;
                            }
                        }

                        #region 單筆訂單批次結束 => 設定推播(成單與否/訂單內容)、APP訊息、寄信(錯誤狀態)

                        var NewStatus = TecoApp.OrderItem.AsEnumerable().Where(p => p.ORDERCODE == OrderItem.CODE).ToList();
                        var OrderItemCount = 0;
                        var GiveawayCount = 0;
                        var OrderItemSuccess = new List<string[]>();
                        var OrderItemFail = new List<string[]>();
                        var GiveawaySuccess = new List<string[]>();
                        var GiveawayFail = new List<string[]>();

                        var NotificationTitle = "";
                        var NotificationMessage = "";
                        var OrderNotificationTitle = "";
                        var OrderNotificationMessage = "";
                        var MailTitle = "";
                        var MailMessage = "";

                        //數量計算
                        foreach (var NewItem in NewStatus)
                        {
                            if (NewItem.ATTRIBUTIONTYPE != "3")
                            {
                                OrderItemCount++;
                                var ProductRList = TecoApp.MARA.AsEnumerable().Where(p => p.MATNR == NewItem.PRODUCTCODE).ToList();
                                var ProductName = ProductRList.FirstOrDefault().MAKTX;

                                if (string.IsNullOrWhiteSpace(NewItem.DELIVERYCODE))
                                {
                                    OrderItemFail.Add(new string[3] {
                                        NewItem.PRODUCTCODE,
                                        ProductName,
                                        NewItem.ERRORMESSAGE
                                    });
                                }
                                else
                                {
                                    OrderItemSuccess.Add(new string[3] {
                                        NewItem.PRODUCTCODE,
                                        ProductName,
                                        NewItem.ERRORMESSAGE
                                    });
                                }
                            }
                            else
                            {
                                GiveawayCount++;
                                var GiveawayList = TecoApp.Giveaway.AsEnumerable().Where(p => p.PRODUCTCODE == NewItem.PRODUCTCODE).ToList();
                                var GiveawayName = GiveawayList.FirstOrDefault().PRODUCTNAME;

                                if (string.IsNullOrWhiteSpace(NewItem.DELIVERYCODE))
                                {
                                    GiveawayFail.Add(new string[3] {
                                        NewItem.PRODUCTCODE,
                                        GiveawayName,
                                        NewItem.ERRORMESSAGE
                                    });
                                }
                                else
                                {
                                    GiveawaySuccess.Add(new string[3] {
                                        NewItem.PRODUCTCODE,
                                        GiveawayName,
                                        NewItem.ERRORMESSAGE
                                    });
                                }
                            }
                        }

                        #region 訊息設定:推播(成單與否/訂單內容)

                        //標題設定
                        NotificationTitle = "東元家電經銷商APP-下單通知";

                        NotificationMessage = "總計下單：" + OrderItemCount + "項商品，" + GiveawayCount + "項贈品。<br>";
                        NotificationMessage += "商品：" + OrderItemSuccess.Count() + "項成功，" + OrderItemFail.Count() + "項失敗。<br>";
                        NotificationMessage += "贈品：" + GiveawaySuccess.Count() + "項成功，" + GiveawayFail.Count() + "項失敗。<br>";
                        NotificationMessage += "(詳細內容請見東元家電經銷商APP)";

                        #endregion 訊息設定:推播(成單與否/訂單內容)

                        #region 訊息設定:APP訊息

                        //標題設定
                        OrderNotificationTitle = "訂單(" + OrderItem.CODE + ")已下單";

                        OrderNotificationMessage = "總計下單：" + OrderItemCount + "項商品，" + GiveawayCount + "項贈品。<br><br>";

                        if (OrderItemSuccess.Count() > 0)
                        {
                            var OrderItemSuccessValue = "";
                            foreach (var Item in OrderItemSuccess)
                            {
                                OrderItemSuccessValue += (OrderItemSuccessValue == "" ? "" : "、<br>");
                                OrderItemSuccessValue += "[" + Item[0] + "]" + Item[1];
                            }
                            OrderNotificationMessage += "成功商品：<br>" + OrderItemSuccessValue + "<br><br>";
                        }

                        if (OrderItemFail.Count() > 0) 
                        {
                            var OrderItemFailValue = "";
                            foreach (var Item in OrderItemFail)
                            {
                                OrderItemFailValue += (OrderItemFailValue == "" ? "" : "、<br>");
                                OrderItemFailValue += "[" + Item[0] + "]" + Item[1];
                            }
                            OrderNotificationMessage += "失敗商品：<br>" + OrderItemFailValue + "<br><br>";
                        }

                        if (GiveawaySuccess.Count() > 0)
                        {
                            var GiveawaySuccessValue = "";
                            foreach (var Item in GiveawaySuccess)
                            {
                                GiveawaySuccessValue += (GiveawaySuccessValue == "" ? "" : "、<br>");
                                GiveawaySuccessValue += "[" + Item[0] + "]" + Item[1];
                            }
                            OrderNotificationMessage += "成功贈品：<br>" + GiveawaySuccessValue + "<br><br>";
                        }

                        if (GiveawayFail.Count() > 0)
                        {
                            var GiveawayFailValue = "";
                            foreach (var Item in GiveawayFail)
                            {
                                GiveawayFailValue += (GiveawayFailValue == "" ? "" : "、<br>");
                                GiveawayFailValue += "[" + Item[0] + "]" + Item[1];
                            }
                            OrderNotificationMessage += "失敗贈品：<br>" + GiveawayFailValue + "<br><br>";
                        }

                        #endregion 訊息設定:APP訊息

                        #region 發送推播/設定APP訊息

                        try 
                        {
                            var NotificationResult = addNotification(OrderItem.CUSTOMER, OrderNotificationTitle, OrderNotificationMessage, NotificationTitle, NotificationMessage);
                        }
                        catch (Exception ex)
                        {
                        }

                        #endregion 發送推播/設定APP訊息

                        #region 訊息設定:寄信(錯誤狀態)

                        //標題設定
                        MailTitle = "東元家電經銷商APP-客戶(" + CustomerName + ")下單通知，訂單編號-" + OrderItem.CODE;

                        MailMessage = "";

                        if (OrderItemSuccess.Count() > 0)
                        {
                            MailMessage += "<table border='1'>";

                            MailMessage += "<tr><td width='450px' bgcolor='#E0E0E0' align='center' colspan='3'>";
                            MailMessage += "下單成功商品：共" + OrderItemSuccess.Count() + "項</td>";

                            MailMessage += "<tr><td width='50px' bgcolor='#E0E0E0' align='center'>";
                            MailMessage += "編號</td>";
                            MailMessage += "<td width='150px' bgcolor='#E0E0E0' align='center'>";
                            MailMessage += "商品號碼</td>";
                            MailMessage += "<td width='250px' bgcolor='#E0E0E0' align='center'>";
                            MailMessage += "商品名稱</td></tr>";

                            var NoCount = 1;
                            foreach (var Item in OrderItemSuccess)
                            {
                                MailMessage += "<tr><td width='50px' align='center'>";
                                MailMessage += NoCount + "</td>";
                                MailMessage += "<td width='150px' align='center'>";
                                MailMessage += Item[0] + "</td>";
                                MailMessage += "<td width='250px' align='center'>";
                                MailMessage += Item[1] + "</td></tr>";
                                NoCount++;
                            }

                            MailMessage += "</table><br><br>";
                        }

                        if (OrderItemFail.Count() > 0) 
                        {
                            MailMessage += "<table border='1'>";

                            MailMessage += "<tr><td width='700px' bgcolor='#E0E0E0' align='center' colspan='4'>";
                            MailMessage += "下單失敗商品：共" + OrderItemFail.Count() + "項</td>";

                            MailMessage += "<tr><td width='50px' bgcolor='#E0E0E0' align='center'>";
                            MailMessage += "編號</td>";
                            MailMessage += "<td width='150px' bgcolor='#E0E0E0' align='center'>";
                            MailMessage += "商品號碼</td>";
                            MailMessage += "<td width='250px' bgcolor='#E0E0E0' align='center'>";
                            MailMessage += "商品名稱</td>";
                            MailMessage += "<td width='250px' bgcolor='#E0E0E0' align='center'>";
                            MailMessage += "失敗原因</td></tr>";

                            var NoCount = 1;
                            foreach (var Item in OrderItemFail)
                            {
                                MailMessage += "<tr><td width='50px' align='center'>";
                                MailMessage += NoCount + "</td>";
                                MailMessage += "<td width='150px' align='center'>";
                                MailMessage += Item[0] + "</td>";
                                MailMessage += "<td width='250px' align='center'>";
                                MailMessage += Item[1] + "</td>";
                                MailMessage += "<td width='250px' align='center'>";
                                MailMessage += Item[2] + "</td></tr>";
                                NoCount++;
                            }

                            MailMessage += "</table><br><br>";
                        }

                        if (GiveawaySuccess.Count() > 0)
                        { 
                            MailMessage += "<table border='1'>";

                            MailMessage += "<tr><td width='450px' bgcolor='#E0E0E0' align='center' colspan='3'>";
                            MailMessage += "下單成功贈品：共" + GiveawaySuccess.Count() + "項</td>";

                            MailMessage += "<tr><td width='50px' bgcolor='#E0E0E0' align='center'>";
                            MailMessage += "編號</td>";
                            MailMessage += "<td width='150px' bgcolor='#E0E0E0' align='center'>";
                            MailMessage += "商品號碼</td>";
                            MailMessage += "<td width='250px' bgcolor='#E0E0E0' align='center'>";
                            MailMessage += "商品名稱</td></tr>";

                            var NoCount = 1;
                            foreach (var Item in GiveawaySuccess)
                            {
                                MailMessage += "<tr><td width='50px' align='center'>";
                                MailMessage += NoCount + "</td>";
                                MailMessage += "<td width='150px' align='center'>";
                                MailMessage += Item[0] + "</td>";
                                MailMessage += "<td width='250px' align='center'>";
                                MailMessage += Item[1] + "</td></tr>";
                                NoCount++;
                            }

                            MailMessage += "</table><br><br>";
                        }

                        if (GiveawayFail.Count() > 0)
                        { 
                            MailMessage += "<table border='1'>";

                            MailMessage += "<tr><td width='700px' bgcolor='#E0E0E0' align='center' colspan='4'>";
                            MailMessage += "下單失敗贈品：共" + GiveawayFail.Count() + "項</td>";

                            MailMessage += "<tr><td width='50px' bgcolor='#E0E0E0' align='center'>";
                            MailMessage += "編號</td>";
                            MailMessage += "<td width='150px' bgcolor='#E0E0E0' align='center'>";
                            MailMessage += "商品號碼</td>";
                            MailMessage += "<td width='250px' bgcolor='#E0E0E0' align='center'>";
                            MailMessage += "商品名稱</td>";
                            MailMessage += "<td width='250px' bgcolor='#E0E0E0' align='center'>";
                            MailMessage += "失敗原因</td></tr>";

                            var NoCount = 1;
                            foreach (var Item in GiveawayFail)
                            {
                                MailMessage += "<tr><td width='50px' align='center'>";
                                MailMessage += NoCount + "</td>";
                                MailMessage += "<td width='150px' align='center'>";
                                MailMessage += Item[0] + "</td>";
                                MailMessage += "<td width='250px' align='center'>";
                                MailMessage += Item[1] + "</td>";
                                MailMessage += "<td width='250px' align='center'>";
                                MailMessage += Item[2] + "</td></tr>";
                                NoCount++;
                            }

                            MailMessage += "</table>";
                        }

                        #endregion 訊息設定:寄信(錯誤狀態)

                        #region 寄信

                        if (OrderItemFail.Count() > 0 || GiveawayFail.Count() > 0)
                        {
                            SMTPRequestModel SMTPRequest = new SMTPRequestModel() { 
                                CustomerID = OrderItem.CUSTOMER,
                                Subject = MailTitle,
                                Body = MailMessage
                            };

                            var SendMailResult = SendMail(SMTPRequest);
                        }

                        #endregion 寄信

                        #endregion 單筆訂單批次結束 => 設定推播(成單與否/訂單內容)、APP訊息、寄信(錯誤狀態)
                    }
                    else
                    {
                        //批次註記
                        OrderItem.BATCH = "N";
                        TecoApp.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("/Order/Batch - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }

                logger.Info("/Order/Batch - End");
            }
        }

        #region 立即推播

        const int PrepareState = 1;
        const int PushingState = 2;
        const int DoneState = 3;
        const int CancelState = 4;
        const int FailState = 5;

        const int TypeAllImmediate = 0;
        const int TypeAllSchedule = 1;
        const int TypeListImmediate = 2;
        const int TypeListSchedule = 3;
        const int TypeOrderMessage = 4;
        public static async Task<bool> addNotification(string userId, string title, string message, string shortTitle, string shortMessage)
        {
            TecoAppEntities db = null;
            bool resultBool = false;
            var now = DateTime.Now;
            var path = ConfigurationManager.AppSettings["SavePushFilePath"] + ConfigurationManager.AppSettings["PushAllFile"];

            try
            {
                db = new TecoAppEntities();

                var result = UploadPushFile(userId.Substring(1));
                path = result.path;

                #region 推播DB

                var orderNotification = new OrderNotification()
                {
                    Title = title,
                    Message = message,
                    Type = 1,
                    CreateTime = now,
                    Tmcode = userId
                };
                db.OrderNotification.Add(orderNotification);

                var Notification = new Notification()
                {
                    Title = shortTitle,
                    Message = shortMessage,
                    CreateTime = now,
                    ScheduleTime = null,
                    Type = TypeListImmediate,
                    State = PrepareState,
                    FilePath = path
                };
                db.Notification.Add(Notification);
                await db.SaveChangesAsync();

                #endregion 推播DB

                PushDirectModel pushDirectModel = new PushDirectModel();
                pushDirectModel.id = Notification.Id;
                pushDirectModel.title = shortTitle;
                pushDirectModel.message = shortMessage;

                HttpStatusCode pushStatusCode = new HttpStatusCode();

                PushBatchModel pushBatchModel = new PushBatchModel();
                pushBatchModel.pushnotification = pushDirectModel;
                pushBatchModel.path = Notification.FilePath;
                pushStatusCode = await PushNotificationService.PushBatch(pushBatchModel);

                if (pushStatusCode != HttpStatusCode.OK)
                {
                    Notification.State = FailState;
                    db.Entry(Notification).State = EntityState.Modified;
                    logger.Error("Java Push Error: " + pushStatusCode);
                    await db.SaveChangesAsync();
                }
                else
                {
                    resultBool = true;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                //throw ex;
                //notificationResBody.Error = ex.ToString();
            }
            finally
            {
                if (db != null) db.Dispose();
            }

            return resultBool;
        }

        public static UploadPushFileResponse UploadPushFile(string userId)
        {
            TecoAppEntities db = null;
            UploadPushFileResponse uploadPushFileResponse = new UploadPushFileResponse();
            var path = "";
            List<string> userIds = new List<string>();

            try
            {
                string newFileName = DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".csv";

                userIds.Add(userId);

                db = new TecoAppEntities();
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions() { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    var device = (from item in db.DEVICEINFO
                                    where userIds.Contains(item.UserId)
                                    select item).OrderByDescending(x => x.LastTime).FirstOrDefault();

                    StringBuilder builder = new StringBuilder();
                    builder.AppendLine(device.DeviceToken);

                    path = ConfigurationManager.AppSettings["SavePushFilePath"] + newFileName;
                    File.WriteAllText(path, builder.ToString());
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw ex;
            }
            finally
            {
                if (db != null) db.Dispose();
            }

            uploadPushFileResponse.userIds = userIds;
            uploadPushFileResponse.path = path;
            return uploadPushFileResponse;
        }

        #endregion 立即推播

        public static Object SendMail(SMTPRequestModel SMTPRequest)
        {
            SMTPResponseModel SMTPResponse = new SMTPResponseModel();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                #region 設定郵件內容

                System.Net.Mail.MailMessage MailMsg = new System.Net.Mail.MailMessage();

                //寄件者
                var SMTPAccount = ConfigurationManager.AppSettings["SMTPAccount"].ToString();
                var SMTPPassword = ConfigurationManager.AppSettings["SMTPPassword"].ToString();
                MailMsg.From = new System.Net.Mail.MailAddress(SMTPAccount, SMTPAccount.Substring(0, SMTPAccount.IndexOf('@')), System.Text.Encoding.UTF8);

                //收件者(測試用)
                MailMsg.To.Add(new System.Net.Mail.MailAddress("nox@webcomm.com.tw", "Nox", System.Text.Encoding.UTF8));
                MailMsg.To.Add(new System.Net.Mail.MailAddress("billy.liu@webcomm.com.tw", "Billy", System.Text.Encoding.UTF8));
                var SMTPTestMail = ConfigurationManager.AppSettings["SMTPTestMail"].ToString();
                MailMsg.To.Add(new System.Net.Mail.MailAddress(SMTPTestMail, SMTPTestMail.Substring(0, SMTPTestMail.IndexOf('@')), System.Text.Encoding.UTF8));

                //收件者
                var CDA = TecoApp.CustomerDepartmentAttribution.AsEnumerable().Where(p => p.CUSTOMERID == SMTPRequest.CustomerID).ToList();
                var ManagerMail = CDA.FirstOrDefault().MANAGEREMAIL.Replace("/", "@").ToLower() + ".com.tw";
                var ManagerName = CDA.FirstOrDefault().MANAGERNAME;
                MailMsg.To.Add(new System.Net.Mail.MailAddress(ManagerMail, ManagerName, System.Text.Encoding.UTF8));

                //主旨
                string MailSubject = SMTPRequest.Subject;
                MailMsg.Subject = MailSubject;
                MailMsg.SubjectEncoding = System.Text.Encoding.UTF8;

                //附件
                //MailMsg.Attachments.Add(new Attachment(new MemoryStream(file.BinContent), file.Name));

                #region 信件內容

                MailMsg.Body = SMTPRequest.Body;

                #endregion 信件內容

                MailMsg.BodyEncoding = System.Text.Encoding.UTF8;
                MailMsg.IsBodyHtml = true;
                MailMsg.Priority = MailPriority.High;

                #endregion 設定郵件內容

                #region 設定郵件物件與寄送

                logger.Info("/Order/SendMail Sendding Subject : {0}", MailSubject);

                // Hinet Smtp
                using (System.Net.Mail.SmtpClient MailObj = new System.Net.Mail.SmtpClient()
                {
                    Credentials = new System.Net.NetworkCredential(SMTPAccount, SMTPPassword),
                    Host = "msr.hinet.net",
                    //Host = "msa.hinet.net",
                    //Port = int.Parse(Port),
                    //EnableSsl = true,
                    DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network
                })
                {
                    MailObj.Send(MailMsg);
                    MailObj.Dispose();
                    MailMsg.Dispose();
                }

                // Google Smtp
                //using (System.Net.Mail.SmtpClient MailObj = new System.Net.Mail.SmtpClient()
                //{
                //    Credentials = new System.Net.NetworkCredential("nocturn.nox.n@gmail.com", "!QAZ@WSX"),
                //    Host = "smtp.gmail.com",
                //    Port = 587,
                //    //EnableSsl = true,
                //    DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network
                //})
                //{
                //    MailObj.Send(MailMsg);
                //    MailObj.Dispose();
                //    MailMsg.Dispose();
                //}

                logger.Info("/Order/SendMail Successful Subject : {0}", MailSubject);

                #endregion 設定郵件物件與寄送

                SMTPResponse.result = "1";
                SMTPResponse.message = "Success";
            }
            catch (Exception ex)
            {
                logger.Error("/Order/SendMail - " + ex);
                SMTPResponse.result = "0";
                SMTPResponse.message = "Exception:/Order/SendMail:" + ex.Message;
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return SMTPResponse;
        }

        #endregion 訂單批次處理

        #endregion 後臺
    }
}
