﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Linq;
using System.Web.Http;
using TecoModels;
using System.Threading.Tasks;
using CsvHelper;
using System.Collections.Generic;
using System.Net.Http;
using System.Net;
using System.IO;
using TecoAPI.Services;
using System.Data.Entity;

namespace TecoAPI.Controllers
{
    public class WareHouseController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region 前臺

        #region 取得倉庫資料

        [HttpPost]
        [CorsHandle]
        public async Task<Object>  Get([FromBody] JObject DATA)
        {
            WareHouseResponseModel WareHouseResponse = new WareHouseResponseModel();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                WareHouseRequestModel WareHouseRequest = JsonConvert.DeserializeObject<WareHouseRequestModel>(JsonConvert.SerializeObject(DATA));
                WareHouseRequest.ADDRESS = WareHouseRequest.ADDRESS.Replace("台", "臺");

                var WareHouseItemList = await TecoApp.WareHouseItem.AsNoTracking().ToListAsync();
                var WareHouseWEIGHT = "";
                var WareHouseID = "";

                #region 設定倉庫資料

                foreach (var WareHouseItem in WareHouseItemList)
                {
                    if (WareHouseRequest.ADDRESS.Contains(WareHouseItem.AREA))
                    {
                        WareHouseWEIGHT = WareHouseItem.WEIGHT;
                        WareHouseID = WareHouseItem.CODE;

                        if (WareHouseWEIGHT == "1")
                        {
                            break;
                        }
                    }
                }

                WareHouseItemList = await TecoApp.WareHouseItem.Where(p => p.CODE == WareHouseID).AsNoTracking().ToListAsync();

                foreach (var WareHouseItem in WareHouseItemList)
                {
                    if (WareHouseRequest.ADDRESS.Contains(WareHouseItem.AREA))
                    {
                        WareHouseWEIGHT = WareHouseItem.WEIGHT;
                        WareHouseID = WareHouseItem.CODE;

                        if (WareHouseWEIGHT == "2")
                        {
                            break;
                        }
                    }
                }

                foreach (var WareHouseItem in WareHouseItemList)
                {
                    if (WareHouseRequest.ADDRESS.Contains(WareHouseItem.AREA))
                    {
                        WareHouseWEIGHT = WareHouseItem.WEIGHT;
                        WareHouseID = WareHouseItem.CODE;

                        if (WareHouseWEIGHT == "3")
                        {
                            break;
                        }
                    }
                }

                #endregion 設定倉庫資料

                //判斷倉庫資料
                if (WareHouseID == "")
                {
                    WareHouseResponse.result = "0";
                    WareHouseResponse.message = "Error:/WareHouse/Get:地址無對應倉庫";
                }
                else
                {
                    WareHouseResponse.result = "1";
                    WareHouseResponse.message = "Success";
                    WareHouseResponse.data.CODE = WareHouseID;
                }
            }
            catch (Exception ex)
            {
                logger.Error("/WareHouse/Get - " + ex.Message);
                WareHouseResponse.result = "0";
                WareHouseResponse.message = "Exception:/WareHouse/Get:" + ex.Message;
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return WareHouseResponse;
        }

        #endregion 取得倉庫資料

        #endregion 前臺

        #region 後臺

        #region 匯入倉庫資料

        [HttpPost]
        [CorsHandle]
        public async Task<List<WHResponseModel>> Import()
        {
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var Config = await TecoApp.Config.Where(p => p.Code == "RootFilePath" && p.Enable == "Y").AsNoTracking().ToListAsync();
                var RootFilePath = Config.FirstOrDefault().Value + "WH\\";

                if (!Request.Content.IsMimeMultipartContent())
                {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                }

                if (!Directory.Exists(RootFilePath))
                {
                    Directory.CreateDirectory(RootFilePath);
                }

                var StreamProvider = new CustomMultipartFormDataStreamProvider(RootFilePath);

                await Request.Content.ReadAsMultipartAsync(StreamProvider);

                foreach (MultipartFileData File in StreamProvider.FileData)
                {
                    string FileName = File.Headers.ContentDisposition.FileName;
                    string FileNameNew = Path.GetFileName(File.LocalFileName);
                    string FilePath;
                    if (FileName.StartsWith("\"") && FileName.EndsWith("\""))
                    {
                        FileName = FileName.Trim('"');
                    }
                    if (FileName.Contains(@"/") || FileName.Contains(@"\"))
                    {
                        FileName = Path.GetFileName(FileName);
                    }
                    FilePath = RootFilePath + FileNameNew;

                    using (var StreamReader = new StreamReader(FilePath))
                    using (var CsvReader = new CsvReader(StreamReader))
                    {
                        CsvReader.Configuration.HasHeaderRecord = false; //不抓欄位名稱，中文會有問題
                        CsvReader.Read(); //跳過第一行欄位名稱
                        var WHUpload = CsvReader.GetRecords<WHUploadModel>().ToList();

                        var WHList = TecoApp.WareHouseItem.ToList();
                        //var OldFirstID = (WHList.Count() == 0 ? 0 : Convert.ToInt32(WHList.FirstOrDefault().ID));
                        //var OldLastID = (WHList.Count() == 0 ? 0 : Convert.ToInt32(WHList.LastOrDefault().ID));

                        TecoApp.WareHouseItem.RemoveRange(WHList);
                        TecoApp.SaveChanges();

                        logger.Debug("/WareHouseItem/Import - RemoveRangeSuccess");

                        //設定新增的ID初始值
                        int ID = 1;
                        //if (OldFirstID < 10000)
                        //{
                        //    ID = OldLastID + 10000;
                        //}

                        //DB新增倉庫清單
                        foreach (var WH in WHUpload)
                        {
                            WareHouseItem log = new WareHouseItem();
                            log.ID = ID;
                            log.CODE = WH.CODE.Trim();
                            log.WEIGHT = WH.WEIGHT.Trim();
                            log.AREA = WH.AREA.Trim();
                            log.NOTE = WH.NOTE.Trim();
                            log.CREATE_TIME = System.DateTime.Now;
                            log.CREATE_USER = "System";
                            TecoApp.WareHouseItem.Add(log);
                            ID++;
                        }

                        TecoApp.SaveChanges();

                        logger.Debug("/WareHouseItem/Import - AddRangeSuccess");
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("/WareHouse/Import - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return await Query();
        }

        #endregion 匯入倉庫資料

        #region 查詢倉庫資料

        [HttpGet]
        [CorsHandle]
        public async Task<List<WHResponseModel>> Query()
        {
            var WHResponse = new List<WHResponseModel>();
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var WHList = await TecoApp.WareHouseItem.AsNoTracking().ToListAsync();

                foreach (var WH in WHList)
                {
                    WHResponse.Add(new WHResponseModel()
                    {
                        ID = WH.ID,
                        CODE = WH.CODE,
                        WEIGHT = WH.WEIGHT,
                        AREA = WH.AREA,
                        NOTE = WH.NOTE
                    });
                }
            }
            catch (Exception ex)
            {
                logger.Error("/WareHouse/Query - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return WHResponse;
        }

        #endregion 查詢倉庫資料

        #endregion 後臺
    }
}
