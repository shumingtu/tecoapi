﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using TecoModels;

namespace TecoAPI.Controllers
{
    public class MARDController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region 庫存查詢

        [HttpPost]
        [CorsHandle]
        public async Task<Object> Query([FromBody] JObject DATA)
        {
            MARDResponseModel Result = new MARDResponseModel();
            TecoAppEntities TecoApp = new TecoAppEntities();
            MARDRequestModel MARDRequest = new MARDRequestModel();

            try
            {
                MARDRequest = JsonConvert.DeserializeObject<MARDRequestModel>(JsonConvert.SerializeObject(DATA));

                var RequestCount = MARDRequest.data.Count();
                var Config = await TecoApp.Config.Where(p => p.Code == "ServiceURL" && p.Enable == "Y").AsNoTracking().ToListAsync();
                var ServiceURL = Config.FirstOrDefault().Value;

                //RFC:庫存查詢
                var GetData = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/MARD/Get", JsonConvert.SerializeObject(MARDRequest));
                MARDResponseModel ResponseItem = JsonConvert.DeserializeObject<MARDResponseModel>(HttpClientHelper.ConvertString(GetData));

                if (ResponseItem.result == "1")
                {
                    foreach (var RespItem in ResponseItem.data)
                    {
                        Result.data.Add(new MARDModel
                        {
                            WERKS = RespItem.WERKS,
                            LGORT = RespItem.LGORT,
                            MATNR = RespItem.MATNR,
                            MAKTX = RespItem.MAKTX,
                            LABST = RespItem.LABST
                        });
                    }

                    Result.result = "1";
                    Result.message = "Success";
                }
                else
                {
                    Result.result = "0";
                    Result.message = ResponseItem.message;
                }
            }
            catch (Exception ex)
            {
                logger.Error("/MARD/Query - " + ex.Message);
                Result.result = "0";
                Result.message = "Error:/MARD/Query:" + ex.Message;
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return Result;
        }

        #endregion 庫存查詢

        #region 庫存轉移

        [HttpPost]
        [CorsHandle]
        public async Task<Object> Transfer([FromBody] JObject DATA)
        {
            TransferResponseModel TransferResponse = new TransferResponseModel();
            TecoAppEntities TecoApp = new TecoAppEntities();
            TransferRequestModel TransferRequest = new TransferRequestModel();

            try
            {
                TransferRequest = JsonConvert.DeserializeObject<TransferRequestModel>(JsonConvert.SerializeObject(DATA));

                var RequestCount = TransferRequest.data.Count();
                var ResponseCount = 0;
                var ResponseErrorCount = 0;
                var Config = await TecoApp.Config.Where(p => p.Code == "ServiceURL" && p.Enable == "Y").AsNoTracking().ToListAsync();
                var ServiceURL = Config.FirstOrDefault().Value;

                //RFC:庫存轉移
                var GetData = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/MARD/Transfer", JsonConvert.SerializeObject(TransferRequest));
                TransferResponse = JsonConvert.DeserializeObject<TransferResponseModel>(HttpClientHelper.ConvertString(GetData));

                if (TransferResponse.result == "1")
                {
                    foreach (var RespItem in TransferResponse.data)
                    {
                        if (string.IsNullOrWhiteSpace(RespItem.MESSAGE))
                        {
                            ResponseCount++;
                        }
                        else
                        {
                            ResponseErrorCount++;
                        }
                    }

                    if (RequestCount == ResponseCount)
                    {
                        TransferResponse.result = "1";
                        TransferResponse.message = "Success";
                    }
                    else
                    {
                        TransferResponse.result = "0";
                        TransferResponse.message = "Error:/MARD/Transfer:移倉失敗商品(" + ResponseErrorCount + "項)";
                    }
                }
                else
                {
                    TransferResponse.result = "0";
                }
            }
            catch (Exception ex)
            {
                logger.Error("/MARD/Transfer - " + ex.Message);
                TransferResponse.result = "0";
                TransferResponse.message = "Error:/MARD/Transfer:" + ex.Message;
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return TransferResponse;
        }

        #endregion 庫存轉移

        #region 庫存更新:RFC=>DB

        [HttpGet]
        public string Update()
        {
            string result = "0";
            TecoAppEntities TecoApp = new TecoAppEntities();

            try
            {
                var Config = TecoApp.Config.AsEnumerable().Where(p => p.Code == "ServiceURL" && p.Enable == "Y").ToList();
                var ServiceURL = Config.FirstOrDefault().Value;
                var WareHouseList = TecoApp.WareHouse.AsEnumerable().ToList();

                //判斷倉庫清單
                if (WareHouseList.Count() > 0)
                {
                    var ProductRList = TecoApp.ProductR.AsEnumerable().ToList();
                    var GiveawayList = TecoApp.Giveaway.AsEnumerable().Where(p => p.START_TIME <= DateTime.Now && p.END_TIME >= DateTime.Now).ToList();

                    //判斷商品清單/贈品清單
                    if (ProductRList.Count() > 0 || GiveawayList.Count() > 0)
                    {
                        MARDRequestModel MARDRequestAll = new MARDRequestModel();
                        MARDRequestModel MARDRequestItem = new MARDRequestModel();
                        MARDResponseModel Response = new MARDResponseModel();

                        //設定庫存查詢清單(商品)
                        if (ProductRList.Count() > 0)
                        {
                            foreach (var ProductItem in ProductRList)
                            {
                                foreach (var WareHouseItem in WareHouseList)
                                {
                                    MARDRequestAll.data.Add(new MARDQueryModel
                                    {
                                        WERKS = "15R0",
                                        LGORT = WareHouseItem.CODE,
                                        MATNR = ProductItem.PRODUCTCODE
                                    });

                                    MARDRequestAll.data.Add(new MARDQueryModel
                                    {
                                        WERKS = "15R0",
                                        LGORT = WareHouseItem.CODE,
                                        MATNR = ProductItem.RELATEDPRODUCTCODE
                                    });
                                }
                            }
                        }

                        //設定庫存查詢清單(贈品)
                        if (GiveawayList.Count() > 0)
                        {
                            foreach (var GiveawayItem in GiveawayList)
                            {
                                foreach (var WareHouseItem in WareHouseList)
                                {
                                    MARDRequestAll.data.Add(new MARDQueryModel
                                    {
                                        WERKS = "12K0",
                                        LGORT = WareHouseItem.CODE,
                                        MATNR = GiveawayItem.PRODUCTCODE
                                    });
                                }
                            }
                        }

                        result = "Query " + MARDRequestAll.data.Count() + "data; ";

                        //庫存查詢
                        int No = 0;
                        int TotalNo = MARDRequestAll.data.Count() + 1;
                        foreach (var ReqItem in MARDRequestAll.data)
                        {
                            No++;
                            TotalNo--;

                            if (No > 10)
                            {
                                No = 1; 
                                MARDRequestItem.data.Clear();
                            }

                            MARDRequestItem.data.Add(new MARDQueryModel
                            {
                                WERKS = ReqItem.WERKS,
                                LGORT = ReqItem.LGORT,
                                MATNR = ReqItem.MATNR
                            });

                            //每10筆資料查詢一次
                            if (No == 10 || TotalNo == 1)
                            {
                                //RFC:庫存查詢
                                var GetData = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/MARD/Get", JsonConvert.SerializeObject(MARDRequestItem));
                                MARDResponseModel ResponseItem = JsonConvert.DeserializeObject<MARDResponseModel>(HttpClientHelper.ConvertString(GetData));

                                if (ResponseItem.result == "1")
                                {
                                    result += "Get " + ResponseItem.data.Count() + "data; ";

                                    foreach (var RespItem in ResponseItem.data)
                                    {
                                        Response.data.Add(new MARDModel
                                        {
                                            WERKS = RespItem.WERKS,
                                            LGORT = RespItem.LGORT,
                                            MATNR = RespItem.MATNR,
                                            MAKTX = RespItem.MAKTX,
                                            LABST = RespItem.LABST
                                        });
                                    }
                                }

                                if (TotalNo != 1)
                                {
                                    System.Threading.Thread.Sleep(1000);
                                }
                            }
                        }

                        //判斷庫存清單
                        if (Response.data.Count() > 0)
                        {
                            var MARDList = TecoApp.MARD.AsEnumerable().ToList();
                            var OldFirstID = (MARDList.Count() == 0 ? 0 : Convert.ToInt32(MARDList.FirstOrDefault().ID));
                            var OldLastID = (MARDList.Count() == 0 ? 0 : Convert.ToInt32(MARDList.LastOrDefault().ID));

                            //設定新增的ID初始值
                            int ID = 1;
                            if (OldFirstID < 10000)
                            {
                                ID = OldLastID + 10000;
                            }

                            //DB新增庫存清單
                            foreach (var MARD in Response.data)
                            {
                                MARD log = new MARD();
                                log.ID = ID;
                                log.WERKS = MARD.WERKS.Trim();
                                log.LGORT = MARD.LGORT.Trim();
                                log.MATNR = MARD.MATNR.Trim();
                                log.MAKTX = MARD.MAKTX.Trim();
                                log.LABST = MARD.LABST.Trim().Replace(".000", "");
                                log.CREATE_TIME = System.DateTime.Now;
                                log.CREATE_USER = "System";
                                TecoApp.MARD.Add(log);
                                ID++;
                            }

                            TecoApp.MARD.RemoveRange(MARDList);
                            TecoApp.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("/MARD/Update - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }
            }

            return result;
        }

        #endregion 庫存更新:RFC=>DB
    }
}
