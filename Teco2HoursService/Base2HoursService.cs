﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using TecoModels;

namespace Teco2HoursService
{
    partial class Base2HoursService : ServiceBase
    {
        private System.Timers.Timer MyTimer;

        public Base2HoursService()
        {
            InitializeComponent();

            this.AutoLog = false;

            if (!System.Diagnostics.EventLog.SourceExists("Base2HoursService"))
            {
                System.Diagnostics.EventLog.CreateEventSource("Base2HoursService", "TecoBatchLog");
            }

            eventLog1.Source = "Base2HoursService";
        }

        protected override void OnStart(string[] args)
        {
            // TODO: 在此加入啟動服務的程式碼。
            MyTimer = new System.Timers.Timer();
            MyTimer.Elapsed += new ElapsedEventHandler(Update);
            MyTimer.Interval = 2 * 60 * 60 * 1000;//2小時
            MyTimer.Start();
        }

        protected override void OnStop()
        {
            // TODO: 在此加入停止服務所需執行的終止程式碼。
            MyTimer.Stop();
            MyTimer = null;
        }

        private void Update(object sender, ElapsedEventArgs e)
        {
            UpdatePortionKNA1();
            Thread.Sleep(3000);

            UpdatePortionCUSTCREDIT();
            Thread.Sleep(3000);

            UpdatePortionMARA();
            Thread.Sleep(3000);

            UpdatePortionCUSTPRICE();
            Thread.Sleep(3000);

            UpdateMARD();
        }

        #region [KNA1]客戶更新:RFC=>DB

        private void UpdatePortionKNA1()
        {
            TecoAppConnection TecoApp = new TecoAppConnection();
            eventLog1.WriteEntry("/KNA1/UpdatePortion - Start");

            try
            {
                var Config = TecoApp.Config.AsEnumerable().Where(p => p.Code == "ServiceURL" && p.Enable == "Y").ToList();
                var ServiceURL = Config.FirstOrDefault().Value;

                //RFC:客戶查詢
                var GetData = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/KNA1/Get", "{\"ALL\":\"\"}");
                KNA1ResponseModel Response = JsonConvert.DeserializeObject<KNA1ResponseModel>(HttpClientHelper.ConvertString(GetData));

                //DB新增客戶清單
                if (Response.result == "1" && Response.data.Count() > 0)
                {
                    var KNA1List = TecoApp.KNA1.AsEnumerable().ToList();

                    var NewIDCount = 1;

                    var NameExist = "";
                    foreach (var KNA1Update in Response.data)
                    {
                        var IsNew = true;

                        foreach (var KNA1 in KNA1List)
                        {
                            if (KNA1.VKORG == KNA1Update.VKORG.Trim() && KNA1.KUNNR == KNA1Update.KUNNR.Trim())
                            {
                                KNA1.NAME1 = KNA1Update.NAME1.Trim();
                                KNA1.VKBUR = KNA1Update.VKBUR.Trim();
                                KNA1.VKGRP = KNA1Update.VKGRP.Trim();
                                KNA1.KDGRP = KNA1Update.KDGRP.Trim();
                                KNA1.BEZEI = KNA1Update.BEZEI.Trim();
                                KNA1.TXNAM_SDB = KNA1Update.TXNAM_SDB.Trim();
                                KNA1.ORT01 = KNA1Update.ORT01.Trim();
                                KNA1.STRAS = KNA1Update.STRAS.Trim();
                                KNA1.UPDATE_TIME = System.DateTime.Now;
                                KNA1.UPDATE_USER = "System";
                                IsNew = false;
                            }
                        }

                        if (IsNew)
                        {
                            KNA1 log = new KNA1();
                            log.ID = KNA1List.LastOrDefault().ID + NewIDCount;
                            NewIDCount++;
                            log.KUNNR = KNA1Update.KUNNR.Trim();
                            log.NAME1 = KNA1Update.NAME1.Trim();
                            log.VKORG = KNA1Update.VKORG.Trim();
                            log.VKBUR = KNA1Update.VKBUR.Trim();
                            log.VKGRP = KNA1Update.VKGRP.Trim();
                            log.KDGRP = KNA1Update.KDGRP.Trim();
                            log.BEZEI = KNA1Update.BEZEI.Trim();
                            log.TXNAM_SDB = KNA1Update.TXNAM_SDB.Trim();
                            log.ORT01 = KNA1Update.ORT01.Trim();
                            log.STRAS = KNA1Update.STRAS.Trim();
                            log.CREATE_TIME = System.DateTime.Now;
                            log.CREATE_USER = "System";
                            TecoApp.KNA1.Add(log);
                        }

                        #region 設定登入清單

                        var AccountExist = false;
                        var KUNNRValue = ReplaceEng(KNA1Update.KUNNR);
                        var SignInList = TecoApp.SignIn.AsEnumerable().ToList();
                        foreach (var SignInItem in SignInList)
                        {
                            if (SignInItem.Account == KUNNRValue)
                            {
                                AccountExist = true;
                                break;
                            }
                        }

                        if (!AccountExist)
                        {
                            if (!NameExist.Contains(KUNNRValue))
                            {
                                SignIn signin = new SignIn();
                                signin.Account = KUNNRValue;
                                signin.Password = KUNNRValue;
                                signin.Name = KNA1Update.NAME1;
                                signin.Device = "";
                                signin.Refresh = "";
                                signin.Enable = "Y";
                                signin.CREATE_TIME = System.DateTime.Now;
                                signin.CREATE_USER = "System";
                                TecoApp.SignIn.Add(signin);
                                NameExist += KUNNRValue + ",";
                            }
                        }

                        #endregion 設定登入清單
                    }

                    TecoApp.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                eventLog1.WriteEntry("/KNA1/UpdatePortion - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }

                eventLog1.WriteEntry("/KNA1/UpdatePortion - End");
            }
        }

        //清除英文字
        private static string ReplaceEng(string value)
        {
            return value.Replace("A", "").Replace("B", "").Replace("C", "")
                .Replace("D", "").Replace("E", "").Replace("F", "")
                .Replace("G", "").Replace("H", "").Replace("I", "")
                .Replace("J", "").Replace("K", "").Replace("L", "")
                .Replace("M", "").Replace("N", "").Replace("O", "")
                .Replace("P", "").Replace("Q", "").Replace("R", "")
                .Replace("S", "").Replace("T", "").Replace("U", "")
                .Replace("V", "").Replace("W", "").Replace("X", "")
                .Replace("Y", "").Replace("Z", "")
                .Replace("a", "").Replace("b", "").Replace("c", "")
                .Replace("d", "").Replace("e", "").Replace("f", "")
                .Replace("g", "").Replace("h", "").Replace("i", "")
                .Replace("j", "").Replace("k", "").Replace("l", "")
                .Replace("m", "").Replace("n", "").Replace("o", "")
                .Replace("p", "").Replace("q", "").Replace("r", "")
                .Replace("s", "").Replace("t", "").Replace("u", "")
                .Replace("v", "").Replace("w", "").Replace("x", "")
                .Replace("y", "").Replace("z", "");
        }

        #endregion [KNA1]客戶更新:RFC=>DB

        #region [CUSTCREDIT]信限更新:RFC=>DB

        private void UpdatePortionCUSTCREDIT()
        {
            TecoAppConnection TecoApp = new TecoAppConnection();
            eventLog1.WriteEntry("/CUSTCREDIT/UpdatePortion - Start");

            try
            {
                var Config = TecoApp.Config.AsEnumerable().Where(p => p.Code == "ServiceURL" && p.Enable == "Y").ToList();
                var ServiceURL = Config.FirstOrDefault().Value;

                //RFC:信限查詢(R)
                var GetData1 = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/CUSTCREDIT/Get", "{\"ALL\":\"\",\"ZKKBER\":\"R000\"}");
                CUSTCREDITResponseModel Response1 = JsonConvert.DeserializeObject<CUSTCREDITResponseModel>(HttpClientHelper.ConvertString(GetData1));

                System.Threading.Thread.Sleep(1000);

                //RFC:信限查詢(K)
                var GetData2 = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/CUSTCREDIT/Get", "{\"ALL\":\"\",\"ZKKBER\":\"K000\"}");
                CUSTCREDITResponseModel Response2 = JsonConvert.DeserializeObject<CUSTCREDITResponseModel>(HttpClientHelper.ConvertString(GetData2));

                //判斷信限清單
                if ((Response1.result == "1" && Response1.data.Count() > 0) ||
                    (Response2.result == "1" && Response2.data.Count() > 0))
                {
                    var CUSTCREDITList = TecoApp.CUSTCREDIT.AsEnumerable().ToList();
                    List<CUSTCREDITModel> UpdateList = new List<CUSTCREDITModel>();

                    var NewIDCount = 1;

                    if (Response1.result == "1" && Response1.data.Count() > 0)
                    {
                        UpdateList.AddRange(Response1.data);
                    }
                    if (Response2.result == "1" && Response2.data.Count() > 0)
                    {
                        UpdateList.AddRange(Response2.data);
                    }

                    //DB新增信限清單
                    foreach (var CUSTCREDITUpdate in UpdateList)
                    {
                        var IsNew = true;

                        foreach (var CUSTCREDIT in CUSTCREDITList)
                        {
                            if (CUSTCREDIT.KKBER == CUSTCREDITUpdate.KKBER.Trim() && CUSTCREDIT.KUNNR == CUSTCREDITUpdate.KUNNR.Trim())
                            {
                                CUSTCREDIT.KLIMK = CUSTCREDITUpdate.KLIMK.Trim().Replace(".00", "");
                                CUSTCREDIT.OBLIG = CUSTCREDITUpdate.OBLIG.Trim().Replace(".00", "");
                                CUSTCREDIT.UPDATE_TIME = System.DateTime.Now;
                                CUSTCREDIT.UPDATE_USER = "System";
                                IsNew = false;
                            }
                        }

                        if (IsNew)
                        {
                            CUSTCREDIT log = new CUSTCREDIT();
                            log.ID = CUSTCREDITList.LastOrDefault().ID + NewIDCount;
                            NewIDCount++;
                            log.KKBER = CUSTCREDITUpdate.KKBER.Trim();
                            log.KUNNR = CUSTCREDITUpdate.KUNNR.Trim();
                            log.KLIMK = CUSTCREDITUpdate.KLIMK.Trim().Replace(".00", "");
                            log.OBLIG = CUSTCREDITUpdate.OBLIG.Trim().Replace(".00", "");
                            log.CREATE_TIME = System.DateTime.Now;
                            log.CREATE_USER = "System";
                            TecoApp.CUSTCREDIT.Add(log);
                        }
                    }

                    TecoApp.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                eventLog1.WriteEntry("/CUSTCREDIT/UpdatePortion - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }

                eventLog1.WriteEntry("/CUSTCREDIT/UpdatePortion - End");
            }
        }

        #endregion [CUSTCREDIT]信限更新:RFC=>DB

        #region [MARA]商品更新:RFC=>DB

        private void UpdatePortionMARA()
        {
            TecoAppConnection TecoApp = new TecoAppConnection();
            eventLog1.WriteEntry("/MARA/UpdatePortion - Start");

            try
            {
                var Config = TecoApp.Config.AsEnumerable().Where(p => p.Code == "ServiceURL" && p.Enable == "Y").ToList();
                var ServiceURL = Config.FirstOrDefault().Value;

                //RFC:商品查詢
                var GetData = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/MARA/Get", "{\"ALL\":\"\"}");
                MARAResponseModel Response = JsonConvert.DeserializeObject<MARAResponseModel>(HttpClientHelper.ConvertString(GetData));

                //DB新增商品清單
                if (Response.result == "1" && Response.data.Count() > 0)
                {
                    var MARAList = TecoApp.MARA.AsEnumerable().ToList();

                    var NewIDCount = 1;

                    foreach (var MARAUpdate in Response.data)
                    {
                        var IsNew = true;

                        foreach (var MARA in MARAList)
                        {
                            if (MARA.MATNR == MARAUpdate.MATNR.Trim())
                            {
                                MARA.MAKTX = MARAUpdate.MAKTX.Trim();
                                MARA.MEINS = MARAUpdate.MEINS.Trim();
                                MARA.MATKL = MARAUpdate.MATKL.Trim();
                                MARA.PRODH = MARAUpdate.PRODH.Trim();
                                MARA.UPDATE_TIME = System.DateTime.Now;
                                MARA.UPDATE_USER = "System";
                                IsNew = false;
                            }
                        }

                        if (IsNew)
                        {
                            MARA log = new MARA();
                            log.ID = MARAList.LastOrDefault().ID + NewIDCount;
                            NewIDCount++;
                            log.MATNR = MARAUpdate.MATNR.Trim();
                            log.MAKTX = MARAUpdate.MAKTX.Trim();
                            log.MEINS = MARAUpdate.MEINS.Trim();
                            log.MATKL = MARAUpdate.MATKL.Trim();
                            log.PRODH = MARAUpdate.PRODH.Trim();
                            log.CREATE_TIME = System.DateTime.Now;
                            log.CREATE_USER = "System";
                            TecoApp.MARA.Add(log);
                        }
                    }

                    TecoApp.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                eventLog1.WriteEntry("/MARA/UpdatePortion - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }

                eventLog1.WriteEntry("/MARA/UpdatePortion - End");
            }
        }

        #endregion [MARA]商品更新:RFC=>DB

        #region [CUSTPRICE]價格更新:RFC=>DB

        private void UpdatePortionCUSTPRICE()
        {
            TecoAppConnection TecoApp = new TecoAppConnection();
            eventLog1.WriteEntry("/CUSTPRICE/UpdatePortion - Start");

            try
            {
                var Config = TecoApp.Config.AsEnumerable().Where(p => p.Code == "ServiceURL" && p.Enable == "Y").ToList();
                var ServiceURL = Config.FirstOrDefault().Value;

                //RFC:價格查詢(R.01)
                var GetData1 = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/CUSTPRICE/Get", "{\"ALL\":\"\",\"ZVKORG\":\"R000\",\"ZVTWEG\":\"01\"}");
                CUSTPRICEResponseModel Response1 = JsonConvert.DeserializeObject<CUSTPRICEResponseModel>(HttpClientHelper.ConvertString(GetData1));

                System.Threading.Thread.Sleep(1000);

                //RFC:價格查詢(R.18)
                var GetData2 = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/CUSTPRICE/Get", "{\"ALL\":\"\",\"ZVKORG\":\"R000\",\"ZVTWEG\":\"18\"}");
                CUSTPRICEResponseModel Response2 = JsonConvert.DeserializeObject<CUSTPRICEResponseModel>(HttpClientHelper.ConvertString(GetData2));

                System.Threading.Thread.Sleep(1000);

                //RFC:價格查詢(K.28)
                var GetData3 = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/CUSTPRICE/Get", "{\"ALL\":\"\",\"ZVKORG\":\"K000\",\"ZVTWEG\":\"28\"}");
                CUSTPRICEResponseModel Response3 = JsonConvert.DeserializeObject<CUSTPRICEResponseModel>(HttpClientHelper.ConvertString(GetData3));

                //判斷價格清單
                if ((Response1.result == "1" && Response1.data.Count() > 0) ||
                    (Response2.result == "1" && Response2.data.Count() > 0) ||
                    (Response3.result == "1" && Response3.data.Count() > 0))
                {
                    var CUSTPRICEList = TecoApp.CUSTPRICE.AsEnumerable().ToList();
                    List<CUSTPRICEModel> UpdateList = new List<CUSTPRICEModel>();

                    var NewIDCount = 1;

                    if (Response1.result == "1" && Response1.data.Count() > 0)
                    {
                        UpdateList.AddRange(Response1.data);
                    }
                    if (Response2.result == "1" && Response2.data.Count() > 0)
                    {
                        UpdateList.AddRange(Response2.data);
                    }
                    if (Response3.result == "1" && Response3.data.Count() > 0)
                    {
                        UpdateList.AddRange(Response3.data);
                    }

                    //DB更新價格清單
                    foreach (var CUSTPRICEUpdate in UpdateList)
                    {
                        var IsNew = true;

                        foreach (var CUSTPRICE in CUSTPRICEList)
                        {
                            if (CUSTPRICE.VKORG == CUSTPRICEUpdate.VKORG.Trim() && CUSTPRICE.VTWEG == CUSTPRICEUpdate.VTWEG.Trim() &&
                                CUSTPRICE.KUNNR == CUSTPRICEUpdate.KUNNR.Trim() && CUSTPRICE.MATNR == CUSTPRICEUpdate.MATNR.Trim())
                            {
                                CUSTPRICE.KAETR = CUSTPRICEUpdate.KAETR.Trim().Replace(".00", "");
                                CUSTPRICE.UPDATE_TIME = System.DateTime.Now;
                                CUSTPRICE.UPDATE_USER = "System";
                                IsNew = false;
                            }
                        }

                        if (IsNew)
                        {
                            CUSTPRICE log = new CUSTPRICE();
                            log.ID = CUSTPRICEList.LastOrDefault().ID + NewIDCount;
                            NewIDCount++;
                            log.VKORG = CUSTPRICEUpdate.VKORG.Trim();
                            log.VTWEG = CUSTPRICEUpdate.VTWEG.Trim();
                            log.KUNNR = CUSTPRICEUpdate.KUNNR.Trim();
                            log.MATNR = CUSTPRICEUpdate.MATNR.Trim();
                            log.KAETR = CUSTPRICEUpdate.KAETR.Trim().Replace(".00", "");
                            log.CREATE_TIME = System.DateTime.Now;
                            log.CREATE_USER = "System";
                            TecoApp.CUSTPRICE.Add(log);
                        }
                    }

                    TecoApp.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                eventLog1.WriteEntry("/CUSTPRICE/UpdatePortion - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }

                eventLog1.WriteEntry("/CUSTPRICE/UpdatePortion - End");
            }
        }

        #endregion [CUSTPRICE]價格更新:RFC=>DB

        #region [MARD]庫存更新:RFC=>DB

        private void UpdateMARD()
        {
            TecoAppConnection TecoApp = new TecoAppConnection();
            eventLog1.WriteEntry("/MARD/Update - Start");

            try
            {
                var Config = TecoApp.Config.AsEnumerable().Where(p => p.Code == "ServiceURL" && p.Enable == "Y").ToList();
                var ServiceURL = Config.FirstOrDefault().Value;
                var WareHouseList = TecoApp.WareHouse.AsEnumerable().ToList();

                //判斷倉庫清單
                if (WareHouseList.Count() > 0)
                {
                    var ProductRList = TecoApp.ProductR.AsEnumerable().ToList();
                    var GiveawayList = TecoApp.Giveaway.AsEnumerable().Where(p => p.START_TIME <= DateTime.Now && p.END_TIME >= DateTime.Now).ToList();

                    //判斷商品清單/贈品清單
                    if (ProductRList.Count() > 0 || GiveawayList.Count() > 0)
                    {
                        MARDRequestModel MARDRequestAll = new MARDRequestModel();
                        MARDRequestModel MARDRequestItem = new MARDRequestModel();
                        MARDResponseModel Response = new MARDResponseModel();

                        //設定庫存查詢清單(商品)
                        if (ProductRList.Count() > 0)
                        {
                            foreach (var ProductItem in ProductRList)
                            {
                                foreach (var WareHouseItem in WareHouseList)
                                {
                                    MARDRequestAll.data.Add(new MARDQueryModel
                                    {
                                        WERKS = "15R0",
                                        LGORT = WareHouseItem.CODE,
                                        MATNR = ProductItem.PRODUCTCODE
                                    });

                                    MARDRequestAll.data.Add(new MARDQueryModel
                                    {
                                        WERKS = "15R0",
                                        LGORT = WareHouseItem.CODE,
                                        MATNR = ProductItem.RELATEDPRODUCTCODE
                                    });
                                }
                            }
                        }

                        //設定庫存查詢清單(贈品)
                        if (GiveawayList.Count() > 0)
                        {
                            foreach (var GiveawayItem in GiveawayList)
                            {
                                foreach (var WareHouseItem in WareHouseList)
                                {
                                    MARDRequestAll.data.Add(new MARDQueryModel
                                    {
                                        WERKS = "12K0",
                                        LGORT = WareHouseItem.CODE,
                                        MATNR = GiveawayItem.PRODUCTCODE
                                    });
                                }
                            }
                        }

                        //庫存查詢
                        int No = 1;
                        foreach (var ReqItem in MARDRequestAll.data)
                        {
                            if (No > 10)
                            {
                                No = 1;
                                MARDRequestItem.data.Clear();
                            }

                            MARDRequestItem.data.Add(new MARDQueryModel
                            {
                                WERKS = ReqItem.WERKS,
                                LGORT = ReqItem.LGORT,
                                MATNR = ReqItem.MATNR
                            });

                            //每10筆資料查詢一次
                            if (No == 10)
                            {
                                //RFC:庫存查詢
                                var GetData = HttpClientHelper.POST_JSON(ServiceURL + "/RFC/MARD/Get", JsonConvert.SerializeObject(MARDRequestItem));
                                MARDResponseModel ResponseItem = JsonConvert.DeserializeObject<MARDResponseModel>(HttpClientHelper.ConvertString(GetData));

                                if (ResponseItem.result == "1")
                                {
                                    foreach (var RespItem in ResponseItem.data)
                                    {
                                        Response.data.Add(new MARDModel
                                        {
                                            WERKS = RespItem.WERKS,
                                            LGORT = RespItem.LGORT,
                                            MATNR = RespItem.MATNR,
                                            MAKTX = RespItem.MAKTX,
                                            LABST = RespItem.LABST
                                        });
                                    }
                                }

                                System.Threading.Thread.Sleep(1000);
                            }

                            No++;
                        }

                        //判斷庫存清單
                        if (Response.data.Count() > 0)
                        {
                            var MARDList = TecoApp.MARD.AsEnumerable().ToList();
                            var OldFirstID = (MARDList.Count() == 0 ? 0 : Convert.ToInt32(MARDList.FirstOrDefault().ID));
                            var OldLastID = (MARDList.Count() == 0 ? 0 : Convert.ToInt32(MARDList.LastOrDefault().ID));

                            //設定新增的ID初始值
                            int ID = 1;
                            if (OldFirstID < 10000)
                            {
                                ID = OldLastID + 10000;
                            }

                            //DB新增庫存清單
                            foreach (var MARD in Response.data)
                            {
                                MARD log = new MARD();
                                log.ID = ID;
                                log.WERKS = MARD.WERKS.Trim();
                                log.LGORT = MARD.LGORT.Trim();
                                log.MATNR = MARD.MATNR.Trim();
                                log.MAKTX = MARD.MAKTX.Trim();
                                log.LABST = MARD.LABST.Trim().Replace(".000", "");
                                log.CREATE_TIME = System.DateTime.Now;
                                log.CREATE_USER = "System";
                                TecoApp.MARD.Add(log);
                                ID++;
                            }

                            TecoApp.MARD.RemoveRange(MARDList);
                            TecoApp.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                eventLog1.WriteEntry("/MARD/Update - " + ex.Message);
            }
            finally
            {
                if (TecoApp != null)
                {
                    TecoApp.Dispose();
                }

                eventLog1.WriteEntry("/MARD/Update - End");
            }
        }

        #endregion [MARD]庫存更新:RFC=>DB
    }
}
