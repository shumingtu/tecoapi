import { Injectable } from '@angular/core';
import * as _ from "lodash";

@Injectable()
export class ButtonAuth {
    canActivate(buttonName) {
        if (localStorage.getItem('token') && localStorage.getItem('loginData')) {
            let loginData = JSON.parse(localStorage.getItem('loginData'));
            return _.includes(loginData.menuFunctions, buttonName);
        }
        return false;
    }
}