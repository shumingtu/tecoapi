import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { Constant } from '../app.constant';
import { CreditLimit } from '../models/creditLimit';
import swal from 'sweetalert2';
import * as moment from 'moment';

@Injectable()
export class CreditLimitService {
    private giftApiUrl = Constant.TECO_API + 'Config/';

    constructor(private http: HttpClient) { }

    getNotifications(): Observable<CreditLimit[]> {
        return this.http.get<CreditLimit[]>(this.giftApiUrl + "Query")
            .pipe(catchError(this.handleError('Query', [])));
    }

    pushNotification(data: any, isNew?: boolean): Observable<any> {
        if (!isNew) {

            const formData: FormData = new FormData();
            formData.append("InfoData", JSON.stringify(data));

            return this.http.put(this.giftApiUrl + "Modify", formData)
                .pipe(catchError(this.handleError('Modify', [])));
        } else {

            const formData: FormData = new FormData();
            formData.append("InfoData", JSON.stringify(data));

            return this.http.post(this.giftApiUrl + "Add", formData)
                .pipe(catchError(this.handleError('Add', [])));
        }
    }


    deleteNotification(notification: CreditLimit | number): Observable<any> {
        const id = typeof notification === 'number' ? notification : notification.ID;
        return this.http.delete(this.giftApiUrl + "Delete/" + id)
            .pipe(catchError(this.handleError('Delete', [])));
    }

    /*deleteNotification(data: any): Observable<any> {
        const formData: FormData = new FormData();
        formData.append("InfoData", JSON.stringify(data));

        return this.http.post(this.giftApiUrl + "Delete", formData)
            .pipe(catchError(this.handleError('Delete', [])));
    }*/

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.error(error);
            swal({
                type: 'error',
                title: '錯誤',
                text: "系統忙碌中"
            });
            return of(result as T);
        };
    }
}
