﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TecoModels.NotificationModel
{
    public class UploadPushFileResponse
    {
        public List<string> userIds { get; set; }
        public string path { get; set; }
    }
}