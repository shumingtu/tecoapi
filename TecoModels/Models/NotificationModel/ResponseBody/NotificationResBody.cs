﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TecoModels.NotificationModel.ResponseBody
{
    public class NotificationResBody
    {
        public int Id { set; get; }
        public string Title { set; get; }
        public string Message { set; get; }
        public DateTime CreateTime { set; get; }
        public DateTime? ScheduleTime { set; get; }
        public DateTime? PushTime { set; get; }
        public string FilePath { set; get; }
        public int Type { set; get; }
        public int State { set; get; }
        public string Error { set; get; }
    }
}