﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TecoModels
{
    public class TransferRequestModel
    {
        public TransferRequestModel()
        {
            this.data = new List<TransferModel>();
        }

        [Display(Name = "資料")]
        public List<TransferModel> data { get; set; }
    }

    public class TransferModel
    {
        public string BWART { get; set; }
        public string WERKS { get; set; }
        public string LGORT { get; set; }
        public string UMLGO { get; set; }
        public string MATNR { get; set; }
        public string MENGE { get; set; }
        public string ITEM_TXT { get; set; }
    }

    public class TransferResponseModel : ResultModel
    {
        public TransferResponseModel()
        {
            this.data = new List<TransferResultModel>();
        }

        [Display(Name = "資料")]
        public List<TransferResultModel> data { get; set; }
    }

    public class TransferResultModel
    {
        public string WERKS { get; set; }
        public string LGORT { get; set; }
        public string UMLGO { get; set; }
        public string MATNR { get; set; }
        public string MENGE { get; set; }
        public string MBLNR { get; set; }
        public string TYPE { get; set; }
        public string MESSAGE { get; set; }
        public string DATUM { get; set; }
        public string UZEIT { get; set; }
    }
}