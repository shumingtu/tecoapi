﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TecoModels
{
    #region 前臺

    public class GiveawayRequestModel
    {
        public string ID { get; set; }
    }

    public class GiveawayResponseModel : ResultModel
    {
        public GiveawayResponseModel()
        {
            this.data = new List<GiveawayModel>();
        }

        [Display(Name = "資料")]
        public List<GiveawayModel> data { get; set; }
    }

    public class GiveawayActivityModel
    {
        public GiveawayActivityModel()
        {
            this.Giveaway = new List<GiveawayModel>();
        }

        public string NAME { get; set; }
        public string DETAIL { get; set; }
        public string STARTTIME { get; set; }
        public string ENDTIME { get; set; }
        public List<GiveawayModel> Giveaway { get; set; }
    }

    public class GiveawayModel
    {
        public string PRODUCTCODE { get; set; }
        public string PRODUCTNAME { get; set; }
        public string PRICE { get; set; }
    }

    #endregion 前臺

    #region 後臺

    public class GQueryResponseModel
    {
        public long ID { get; set; }
        public string PRODUCTCODE { get; set; }
        public string PRODUCTNAME { get; set; }
        public string PRICE { get; set; }
        public string TYPE { get; set; }
        public DateTime START_TIME { get; set; }
        public DateTime END_TIME { get; set; }
    }

    public class GAddRequestModel
    {
        public string PRODUCTCODE { get; set; }
        public string PRODUCTNAME { get; set; }
        public string PRICE { get; set; }
        public string TYPE { get; set; }
        public DateTime START_TIME { get; set; }
        public DateTime END_TIME { get; set; }
    }

    public class GModifyRequestModel
    {
        public long ID { get; set; }
        public string PRODUCTCODE { get; set; }
        public string PRODUCTNAME { get; set; }
        public string PRICE { get; set; }
        public string TYPE { get; set; }
        public DateTime START_TIME { get; set; }
        public DateTime END_TIME { get; set; }
    }

    public class GDeleteRequestModel
    {
        public long ID { get; set; }
    }

    #endregion 後臺
}