﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TecoModels
{
    public class ScheduleRequestModel
    {
        public ScheduleRequestModel()
        {
        }

        public string Function { get; set; }
        public string Schedule { get; set; }
    }
}