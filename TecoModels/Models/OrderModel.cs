﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TecoModels
{
    #region Add

    public class OrderRequestModel
    {
        public OrderRequestModel()
        {
            this.data = new List<OrderItemModel>();
        }

        public string ID { get; set; }
        public string DEPARTMENT { get; set; }
        public string WAREHOUSE { get; set; }
        public string TYPE { get; set; }
        public string PURCHASEORDERCODE { get; set; }
        public string CITY { get; set; }
        public string ADDRESS { get; set; }
        public string PHONE { get; set; }
        public string MOBILE { get; set; }
        public string RECEIVER { get; set; }
        public string DELIVERYINSTRUCTIONCODE { get; set; }
        public string NOTE { get; set; }
        public string SHIPPINGTIME { get; set; }

        [Display(Name = "資料")]
        public List<OrderItemModel> data { get; set; }
    }

    public class OrderItemModel
    {
        public OrderItemModel()
        {
            this.Giveaway = new List<GiveawayItemModel>();
        }

        public string PRICETYPE { get; set; }
        public string PRODUCTCODE { get; set; }
        public string PRODUCTQUANTITY { get; set; }
        public string RELATEDPRODUCTCODE { get; set; }
        public string RELATEDPRODUCTQUANTITY { get; set; }

        [Display(Name = "贈品資料")]
        public List<GiveawayItemModel> Giveaway { get; set; }
    }

    public class GiveawayItemModel
    {
        public string PRODUCTCODE { get; set; }
        public string PRODUCTQUANTITY { get; set; }
    }

    public class OrderRFCRequestModel
    {
        public OrderRFCRequestModel()
        {
            this.data = new List<OrderRFCItemModel>();
        }

        public string ZAUART { get; set; }
        public string ZVKORG { get; set; }
        public string ZVTWEG { get; set; }
        public string ZSPART { get; set; }
        public string ZKUNNR { get; set; }
        public string ZKUNWE { get; set; }
        public string ZBSTKD { get; set; }
        public string ZCITY1 { get; set; }
        public string ZSTREET { get; set; }
        public string ZTELF1 { get; set; }
        public string ZTEL_NUMBER { get; set; }
        public string ZANRED { get; set; }
        public string ZMARK { get; set; }

        [Display(Name = "資料")]
        public List<OrderRFCItemModel> data { get; set; }
    }

    public class OrderRFCItemModel
    {
        public string POSNR { get; set; }
        public string MATNR { get; set; }
        public string WERKS { get; set; }
        public string LGORT { get; set; }
        public string KWMENG { get; set; }
        public string CO_NO { get; set; }
        public string CO_SEQ { get; set; }
    }

    public class OrderResponseModel : ResultModel
    {
        public OrderResponseModel()
        {
            this.data = new List<OrderErrorItemModel>();
        }

        public string SALESCODE { get; set; }
        public string DELIVERYCODE { get; set; }
        public string POSTINGCODE { get; set; }
        public string INVOICECODE { get; set; }

        [Display(Name = "失敗資料")]
        public List<OrderErrorItemModel> data { get; set; }
    }

    public class OrderErrorItemModel
    {
        public string CO_NO { get; set; }
        public string CO_SEQ { get; set; }
        public string PRODUCTNO { get; set; }
        public string PRODUCTCODE { get; set; }
        public string PRODUCTQUANTITY { get; set; }
        public string ERRORTYPE { get; set; }
        public string ERRORMESSAGE { get; set; }
        public string ERRORDATE { get; set; }
        public string ERRORTIME { get; set; }
    }

    #endregion Add

    #region Lock

    public class OrderLockRequestModel
    {
        public string ID { get; set; }
        public string CODE { get; set; }
    }

    #endregion Lock

    #region Modify

    public class OrderModifyRequestModel
    {
        public OrderModifyRequestModel()
        {
            this.data = new List<OrderModifyItemModel>();
        }

        public string ID { get; set; }
        public string CODE { get; set; }
        public string DEPARTMENT { get; set; }
        public string WAREHOUSE { get; set; }
        public string TYPE { get; set; }
        public string PURCHASEORDERCODE { get; set; }
        public string CITY { get; set; }
        public string ADDRESS { get; set; }
        public string PHONE { get; set; }
        public string MOBILE { get; set; }
        public string RECEIVER { get; set; }
        public string DELIVERYINSTRUCTIONCODE { get; set; }
        public string NOTE { get; set; }
        public string SHIPPINGTIME { get; set; }

        [Display(Name = "資料")]
        public List<OrderModifyItemModel> data { get; set; }
    }

    public class OrderModifyItemModel
    {
        public OrderModifyItemModel()
        {
            this.Giveaway = new List<GiveawayModifyItemModel>();
        }

        public string PRICETYPE { get; set; }
        public string PRODUCTCODE { get; set; }
        public string PRODUCTQUANTITY { get; set; }
        public string RELATEDPRODUCTCODE { get; set; }
        public string RELATEDPRODUCTQUANTITY { get; set; }

        [Display(Name = "贈品資料")]
        public List<GiveawayModifyItemModel> Giveaway { get; set; }
    }

    public class GiveawayModifyItemModel
    {
        public string PRODUCTCODE { get; set; }
        public string PRODUCTQUANTITY { get; set; }
    }

    #endregion Modify

    #region Delete

    public class OrderDeleteRequestModel
    {
        public string ID { get; set; }
        public string CODE { get; set; }
    }

    #endregion Delete

    #region Get

    public class OrderGetRequestModel
    {
        public string CODE { get; set; }
        public string DELIVERYCODE { get; set; }
        public string CUSTOMER { get; set; }
        public DateTime? START_TIME { get; set; }
        public DateTime? END_TIME { get; set; }
    }

    #endregion Get

    #region Query

    public class OrderQueryRequestModel
    {
        public string ID { get; set; }
    }

    public class OrderQueryResponseModel : ResultModel
    {
        public OrderQueryResponseModel()
        {
            this.data = new List<OrderQueryItemModel>();
        }

        [Display(Name = "資料")]
        public List<OrderQueryItemModel> data { get; set; }
    }

    public class OrderQueryItemModel
    {
        public OrderQueryItemModel()
        {
            this.ProductData = new List<OrderQueryProductModel>();
        }

        public string ID { get; set; }
        public string DEPARTMENT { get; set; }
        public string WAREHOUSE { get; set; }
        public string TYPE { get; set; }
        public string PURCHASEORDERCODE { get; set; }
        public string CITY { get; set; }
        public string ADDRESS { get; set; }
        public string PHONE { get; set; }
        public string MOBILE { get; set; }
        public string RECEIVER { get; set; }
        public string DELIVERYINSTRUCTIONCODE { get; set; }
        public string NOTE { get; set; }
        public string SHIPPINGTIME { get; set; }
        public string CODE { get; set; }
        public string LOCKED { get; set; }
        public string BATCH { get; set; }
        public string STATUS { get; set; }
        public string MESSAGE { get; set; }
        public DateTime CREATE_TIME { get; set; }

        [Display(Name = "資料")]
        public List<OrderQueryProductModel> ProductData { get; set; }
    }

    public class OrderQueryProductModel
    {
        public OrderQueryProductModel()
        {
            this.RELATEDPRODUCTCODE = "";
            this.RELATEDPRODUCTQUANTITY = "";
            this.Giveaway = new List<GiveawayQueryItemModel>();
        }

        public string PRICETYPE { get; set; }
        public string PRODUCTCODE { get; set; }
        public string PRODUCTQUANTITY { get; set; }
        public string SHIPPINGTIME { get; set; }
        public string RELATEDPRODUCTCODE { get; set; }
        public string RELATEDPRODUCTQUANTITY { get; set; }
        public string RELATEDSHIPPINGTIME { get; set; }
        public string SALESCODE { get; set; }
        public string DELIVERYCODE { get; set; }
        public string POSTINGCODE { get; set; }
        public string INVOICECODE { get; set; }
        public string ERRORTYPE { get; set; }
        public string ERRORMESSAGE { get; set; }
        public string ERRORDATE { get; set; }
        public string ERRORTIME { get; set; }
        public string DELIVERYSTATUS { get; set; }

        [Display(Name = "贈品資料")]
        public List<GiveawayQueryItemModel> Giveaway { get; set; }
    }

    public class GiveawayQueryItemModel
    {
        public string PRODUCTCODE { get; set; }
        public string PRODUCTQUANTITY { get; set; }
        public string SHIPPINGTIME { get; set; }
        public string SALESCODE { get; set; }
        public string DELIVERYCODE { get; set; }
        public string POSTINGCODE { get; set; }
        public string INVOICECODE { get; set; }
        public string ERRORTYPE { get; set; }
        public string ERRORMESSAGE { get; set; }
        public string ERRORDATE { get; set; }
        public string ERRORTIME { get; set; }
    }


    #endregion Query
}