﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TecoModels
{
    #region 後臺

    public class ConfigQueryResponseModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Value { get; set; }
        public string Enable { get; set; }
    }

    public class ConfigAddRequestModel
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Value { get; set; }
        public string Enable { get; set; }
    }

    public class ConfigModifyRequestModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Value { get; set; }
        public string Enable { get; set; }
    }

    public class ConfigDeleteRequestModel
    {
        public long ID { get; set; }
    }

    #endregion 後臺
}