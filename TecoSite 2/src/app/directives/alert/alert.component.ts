import { Component, OnInit } from '@angular/core';
import { AlertService } from '../../services/alertService';

@Component({
    selector: 'app-alert',
    templateUrl: './alert.component.html',
    styleUrls: ['./alert.component.css']
})
export class AlertComponent {
    message: any;
    constructor(private alertService: AlertService) { }

    ngOnInit() {
        this.alertService.getMessage().subscribe(message => {
            this.message = message;
            if (message) {
                setTimeout(() => this.alertService.clear(), 4000);
            }
        });
    }
}
