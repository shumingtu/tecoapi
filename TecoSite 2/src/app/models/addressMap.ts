export class AddressMap {
    ID: number;
    CODE: string;
    WEIGHT: string;
    AREA: string;
    NOTE: string;
}