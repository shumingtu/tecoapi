import { Injectable, } from '@angular/core';
import { Location } from '@angular/common';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AlertService } from '../services/alertService';
import { Router } from '@angular/router';
import 'rxjs/add/operator/do';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(
        private alertService: AlertService,
        private router: Router
    ) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // let token = JSON.parse(localStorage.getItem('token'));
        // if (token) {
        //     if (request.url.indexOf('jsonip') <= 0) {
        //         request = request.clone({
        //             setHeaders: {
        //                 Authorization: `Bearer ${token}`
        //             }
        //         });
        //     }
        // }
        return next.handle(request).do(event => {
            if (event.type != 0) {
                switch (request.method) {
                    case 'GET':
                        break;
                    case 'POST':
                        if (request.url.indexOf('/user/list') >= 0 ||
                            request.url.indexOf('/operationLog/list') >= 0) {
                            // this.alertService.success("篩選成功");
                        } else {
                            this.alertService.success("新增成功");
                        }
                        break;
                    case 'PUT':
                        this.alertService.success("修改成功");
                        break;
                    case 'DELETE':
                        this.alertService.success("刪除成功");
                        break;
                    default:
                        break;
                }
            }
        }, err => {
            if (err instanceof HttpErrorResponse) {
                let errMessage;
                switch (err.status) {
                    case 0:
                        errMessage = '系統忙碌中';
                        break;
                    case 401:
                        errMessage = (err.error.message) ? (err.error.message) : err.error;
                        break;
                    case 500:
                        errMessage = '系統忙碌中';
                        break;
                    default:
                        errMessage = '系統忙碌中';
                        break;
                }
                if (errMessage.indexOf('權限已改變') >= 0) {
                    this.router.navigate(['login']);
                    this.alertService.refreshLoginState(false);
                } else if (errMessage.indexOf('帳號已停用') >= 0) {
                    this.router.navigate(['login']);
                    this.alertService.refreshLoginState(false);
                }
                this.alertService.error(errMessage);
            }
        });
    }
}