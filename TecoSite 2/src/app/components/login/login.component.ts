import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../services/authenticationService';
import { AlertService } from '../../services/alertService';
import swal from 'sweetalert2';
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService
    ) { }

    public loginForm: FormGroup;
    public errorMsg;
    public loading = false;
    @Input() public isLogin = false;
    ngOnInit() {
        this.authenticationService.logout();
        this.loginForm = new FormGroup({
            'username': new FormControl('', Validators.required),
            'password': new FormControl('', Validators.required),
        });
    }
    public Login(): void {
        this.loading = true;
        this.errorMsg = null;
        localStorage.setItem('token', "testToken");
        localStorage.setItem('loginData', JSON.stringify({ username: "TecoAdmin"}));
        this.router.navigate(['']);
        // this.authenticationService.login(this.loginForm.value['username'], this.loginForm.value['password'])
        //     .subscribe((user) => {
        //         if (user && user.token && user.loginData) {
        //             localStorage.setItem('token', JSON.stringify(user.token));
        //             localStorage.setItem('loginData', JSON.stringify(user.loginData));
        //             this.router.navigate(['']);
        //         }
        //     }, (error) => {
        //         this.loading = false;
        //         this.loginForm.value['username'] = null;
        //         this.loginForm.value['password'] = null;
        //         swal({
        //             type: 'error',
        //             title: '錯誤',
        //             text: '帳密錯誤'
        //         });
        //         this.loginForm.reset();
        //     });
    }
}
