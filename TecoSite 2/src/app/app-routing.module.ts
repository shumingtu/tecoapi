import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { AccountManagerComponent } from './components/accountManager/accountManager.component';
import { NotificationManagerComponent } from './components/notificationManager/notificationManager.component';
import { AddressMapManagerComponent } from './components/addressMapManager/addressMapManager.component';
import { OrderManagerComponent } from './components/orderManager/orderManager.component';
import { ProductListManagerComponent } from './components/productListManager/productListManager.component';
import { WorkdayManagerComponent } from './components/workdayManager/workdayManager.component';
import { CreditLimitManagerComponent } from './components/creditLimitManager/creditLimitManager.component';
import { GiftManagerComponent } from './components/giftManager/giftManager.component';
import { AdvertisementManagerComponent } from './components/advertisementManager/advertisementManager.component';
import { CustomerDataManagerComponent } from './components/customerDataManager/customerDataManager.component';

const routes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    { path: 'home', component: HomeComponent },
    //{ path: 'account', component: AccountManagerComponent },
    { path: 'notification', component: NotificationManagerComponent },
    { path: 'addressMap', component: AddressMapManagerComponent },
    { path: 'order', component: OrderManagerComponent },
    { path: 'productList', component: ProductListManagerComponent },
    { path: 'workday', component: WorkdayManagerComponent },
    { path: 'gift', component: GiftManagerComponent },
    { path: 'creditLimit', component: CreditLimitManagerComponent },
    { path: 'advertisement', component: AdvertisementManagerComponent },
    { path: 'customerData', component: CustomerDataManagerComponent },
    // { path: 'home', component: HomeComponent, canActivate: [Auth] },
    // { path: 'role', component: RoleManagerComponent, canActivate: [Auth] },
    // { path: 'account', component: AccountManagerComponent, canActivate: [Auth] },
    // { path: 'notification', component: NotificationManagerComponent, canActivate: [Auth] },
    // { path: 'transLog', component: TransLogComponent, canActivate: [Auth] },
    // { path: 'ticketManager', component: TicketManagerComponent, canActivate: [Auth] },
    // { path: 'checkManager', component: CheckManagerComponent, canActivate: [Auth] },
    // { path: 'contactManager', component: ContactManagerComponent, canActivate: [Auth] },
    // { path: 'advertisementManager', component: AdvertisementManagerComponent, canActivate: [Auth] },
    { path: '**', redirectTo: '/home' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: true })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
