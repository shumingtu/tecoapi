//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace TecoService
{
    using System;
    using System.Collections.Generic;
    
    public partial class MARA
    {
        public long ID { get; set; }
        public string MATNR { get; set; }
        public string MAKTX { get; set; }
        public string MEINS { get; set; }
        public string MATKL { get; set; }
        public string PRODH { get; set; }
        public System.DateTime CREATE_TIME { get; set; }
        public string CREATE_USER { get; set; }
        public Nullable<System.DateTime> UPDATE_TIME { get; set; }
        public string UPDATE_USER { get; set; }
    }
}
